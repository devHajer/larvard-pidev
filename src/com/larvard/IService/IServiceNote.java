/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import java.sql.SQLException;
import java.util.List;
import com.larvard.Entity.Note ;
/**
 *
 * @author ivaxs
 */
public interface IServiceNote<T> {
    
     public void ajouter(T t) throws SQLException ;
     void delete(int t) throws SQLException ;
     //void updateNomColonne(String mati, String mat) throws SQLException ;
      List<T> readAll() throws SQLException ; 
    
}
