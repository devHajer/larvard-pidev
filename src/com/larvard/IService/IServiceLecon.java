/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
/**
 *
 * @author ivaxs
 */
public interface IServiceLecon<T> {
      void ajouter(T t) throws SQLException , ParseException;
    void delete(int e) throws SQLException ;
    void update(T t , String nom) throws SQLException ;
     public List<T> readAll() throws SQLException ;
}
