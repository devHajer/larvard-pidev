/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import com.larvard.Entity.Evaluation;
/**
 *
 * @author ivaxs
 */
public interface IServiceEvaluation <T> {
     void ajouter(T t) throws SQLException , ParseException;
    void delete(int e) throws SQLException ;
   void updateapr(int id ,int deci) throws SQLException ;
     public List<T> readAll() throws SQLException ;
    
}
