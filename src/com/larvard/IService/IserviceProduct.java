/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;

import com.larvard.Entity.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author mouhamed
 */
public interface IserviceProduct<T> {
        void AjoutProduit(T t) throws SQLException;
    boolean deleteProduit(Product p) throws SQLException;
    boolean updateProduit(Product p) throws SQLException;
    List<T> readAllProduit() throws SQLException;
    public List<T>searchProductByID(String cat) throws SQLException;
}
