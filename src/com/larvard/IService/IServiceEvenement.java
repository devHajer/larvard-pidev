/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import com.larvard.Entity.Evenement;
/**
 *
 * @author ivaxs
 */
public interface IServiceEvenement <T>{
     //void ajouter(T t) throws SQLException;
    void ajouter(T t) throws SQLException,ParseException;
    boolean delete(T t) throws SQLException;
    boolean update(T t) throws SQLException;
    List<T> readAll() throws SQLException;
    List<T> SearchEvenementByName(String t)throws SQLException;
    boolean delete1(int i) throws SQLException;
}
