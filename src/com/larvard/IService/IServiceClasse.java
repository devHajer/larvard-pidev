/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import com.larvard.Entity.Classe ;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author ivaxs
 */
public interface IServiceClasse<T> {
        void ajouter(T c) throws SQLException ;
     void delete(int t) throws SQLException ;
     void updateNomClasse(int  t,String nom) throws SQLException ;
  List<T> readAll() throws SQLException ; 
   List<Classe> SearchClasseByName(String nom) throws SQLException;
    
}
