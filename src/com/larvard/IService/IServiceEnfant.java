/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import com.larvard.Entity.Enfant;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
/**

/**
 *
 * @author ivaxs
 */
public interface IServiceEnfant<T> {
    void ajouter(T t) throws SQLException , ParseException;
    void delete(int t) throws SQLException;
    void updateIMG(int t,String img) throws SQLException;
    List<T> readAll();
    List<Enfant> readnomprdate() throws SQLException;
     void updateAfterSchool(int ida, int ide);
    
}
