/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import com.larvard.Entity.Activite;
import java.sql.SQLException;
import java.util.List;
import com.larvard.Service.ServiceActivite ;
 
/**
 *
 * @author ivaxs
 */
public interface IServiceActivite<T> {
    void ajouter(T t) throws SQLException;
      void delete(int t) throws SQLException;
      boolean supp(T t) throws SQLException ;
      void updateDescription(int t,String desc) throws SQLException;
       List<Activite> readAll() throws SQLException ;
      List<String> readTitre();
     // List<String> readTitre(int i);
      public String getTitre(int i) ;
}
