/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import com.larvard.Entity.User ;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
/**
 *
 * @author ivaxs
 */
public interface IServiceUsers <T>{
         void AjoutUsers(T t) throws SQLException , ParseException;
   void deleteUsers(int  cin) throws SQLException;
    void updateUsers(int id) throws SQLException;
    List<T> readAllUsers() throws SQLException;
}
