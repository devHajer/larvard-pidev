/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


/**
 *
 * @author ivaxs
 */
public interface IServiceAfterSchool<T> {
      void ajouter(T t) throws SQLException , ParseException;
    void delete(int t) throws SQLException;
    List<T> readAll() throws SQLException;
  List<Integer> readid();
    
}
