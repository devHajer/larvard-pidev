/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.IService;
import com.larvard.Entity.Reclamation;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


/**
 *
 * @author ivaxs
 */
public interface IServiceReclamation<T> {
      void ajouter(T t) throws SQLException , ParseException;
    void delete(int t) throws SQLException;
    void updatetat(int t,boolean etat) throws SQLException;
    List<T> readAll() throws SQLException ;
    
}
