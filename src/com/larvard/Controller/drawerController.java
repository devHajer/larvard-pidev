/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class drawerController implements Initializable {

    @FXML
    private AnchorPane box;
    @FXML
    private ImageView imgUser;
    @FXML
    private Label labelUser;
    @FXML
    private JFXButton acceuil;
    @FXML
    private JFXButton listproduit;
    @FXML
    private JFXButton categorie;
    @FXML
    private JFXButton panier;
    @FXML
    private JFXButton profile;
    @FXML
    private JFXButton deconnexion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void makeAccueil(ActionEvent event) {
    }


    @FXML
    private void makeCompte(ActionEvent event) {
    }

    @FXML
    private void makeDisconnect(ActionEvent event) {
    }

    @FXML
    private void makeProduit(ActionEvent event) throws IOException {
        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));             

    }

    @FXML
    private void makeCategorie(ActionEvent event) throws IOException {

    }

    @FXML
    private void makePanier(ActionEvent event) throws IOException {
    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/cartpage.fxml"))));             

    }
    
}
