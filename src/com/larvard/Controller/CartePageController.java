/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import com.larvard.Entity.User;
import com.larvard.Entity.Commande;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import com.larvard.Entity.Product;
import static com.larvard.Entity.Product.Panier;
import static com.larvard.Entity.Product.getPanier;
import com.larvard.Service.ConcatPDFFiles;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import com.larvard.Service.Pdf;
import javafx.scene.text.Text;
import com.larvard.Service.ServiceCommande;
import com.larvard.Service.ServiceProduct;
import com.larvard.test.LarvardPI;

/**
 *
 * @author mouhamed
 */

public class CartePageController implements Initializable {
    private User user;
    
    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    @FXML
    private AnchorPane anchorpane;
    @FXML
    private Pane pane;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private Text prix_total;
    @FXML
    private Button valider;

    ServiceProduct service_pr = new ServiceProduct();
    ServiceCommande service_commande = new ServiceCommande();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            AnchorPane box = FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AccueilFXML.fxml"));
            drawer.open();
            drawer.setSidePane(box);

            HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(hamburger);
            transition.setRate(1);
            hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
                transition.setRate(transition.getRate() * 1);
                transition.play();

                if (drawer.isShown()) {
                    drawer.close();
                } else {
                    drawer.open();
                }

            });
        } catch (IOException ex) {
            System.out.println(ex);
            // Logger.getLogger(CartePageController.this).log(Level.SEVERE, null, ex);
        }
        ObservableList<Product> panier = FXCollections.observableArrayList();
        for (Product p : getPanier()) {
            panier.add(p);
        }
        cart_table(panier);
    }

    public void cart_table(ObservableList<Product> panier) {
        ObservableList<Product> panier2 = FXCollections.observableArrayList();
        for (Product p : panier2) {
            panier.add(p);
        }
        int prixtotal = 0;
        int prixtotal_cell = 0;
        int k = 0;
        int total_p = 0;
        int i = 0;
        int width = 0;

        //pane pour les produit selectione
        ObservableList<Product> achat = FXCollections.observableArrayList();
        Pane pane = new Pane();
        pane.setLayoutX(269);
        pane.setLayoutY(106);
        anchorpane.getChildren().add(pane);
        while (i < panier.size()) //for (int i = 0; i < panier.size(); i++) 
        {

            Product get = panier.get(i);

            achat.add(get);
            prixtotal = (int) (prixtotal + (get.getQuantite() * get.getPrice()));
            prix_total.setText(String.valueOf(prixtotal));
            prixtotal_cell = (int) (get.getQuantite() * get.getPrice());
            Group group = new Group();
            group.setLayoutX(0);
            HBox hbglobal = new HBox();
            HBox hb1 = new HBox();
            HBox hb1_1 = new HBox();
            HBox hb2 = new HBox();
            HBox hb3 = new HBox();
            HBox hb4 = new HBox();
            HBox hb5 = new HBox();
            hbglobal.setLayoutX(0);
            hb1.setLayoutX(0);
            hb1_1.setLayoutX(50);
            hb2.setLayoutX(177);
            hb3.setLayoutX(305);
            hb4.setLayoutX(412);
            hb5.setLayoutX(525);
            hbglobal.setLayoutY(20 + k);
            hb1.setLayoutY(65 + k);
            hb1_1.setLayoutY(65 + k);
            hb2.setLayoutY(65 + k);
            hb3.setLayoutY(65 + k);
            hb4.setLayoutY(65 + k);
            hb5.setLayoutY(65 + k);
            /**
             * ********************************
             */
            FontAwesomeIconView delete = new FontAwesomeIconView(FontAwesomeIcon.TIMES_CIRCLE);
            delete.setSize("20");
            delete.setFill(Color.RED);

            delete.setOnMouseClicked((MouseEvent event) -> {
                pane.getChildren().remove(group);
                anchorpane.getChildren().remove(pane);
                achat.remove(get);
                int a = Integer.parseInt(prix_total.getText());
                prix_total.setText(String.valueOf(a - (get.getQuantite() * get.getPrice())));
                AfficheProduitFrontController.nombreproduits = AfficheProduitFrontController.nombreproduits - get.getQuantite();
                cart_table(achat);
            });

            String A = get.getImg();
            A = "C:\\xampp\\htdocs\\imgproj\\" + A;
            File F1 = new File(A);
            Image image2 = new Image(F1.toURI().toString());
            ImageView image = new ImageView();
            image.setFitWidth(60);
            image.setFitHeight(60);
            image.setImage(image2);
            Text t2 = new Text(get.getNom());
            Text t3 = new Text(String.valueOf(get.getPrice()));
            TextField t4 = new TextField(String.valueOf(get.getQuantite()));
            t4.setPrefWidth(45);
            total_p = (int) (get.getPrice() * get.getQuantite());
            Button moin = new Button("-");
            Button plus = new Button("+");
            Text t5 = new Text(String.valueOf(total_p));
            moin.setOnMouseClicked((MouseEvent event) -> {
                int qnt = Integer.parseInt(t4.getText());
                achat.remove(get);
                qnt = qnt - 1;
                t4.setText(String.valueOf(qnt));
                t5.setText(String.valueOf(qnt * get.getPrice()));
                Double a = Double.parseDouble(prix_total.getText());
                prix_total.setText(String.valueOf(a - get.getPrice()));
                get.setQuantite(qnt);
                AfficheProduitFrontController.nombreproduits--;
                achat.add(get);
                
               Product.Panier = achat;
               /// IF QUANTITE == 0 LE PRODUIT SERA SUPPRIMER DU PANIER
                if (qnt == 0) {
                    anchorpane.getChildren().remove(pane);
                    achat.remove(get);
                    prix_total.setText(String.valueOf(a - get.getPrice()));
                    System.out.println("achat after delete" + achat);
                    cart_table(achat);
                }
            });
            plus.setOnMouseClicked((MouseEvent event) -> {
                int qnt = Integer.parseInt(t4.getText());
                qnt = qnt + 1;
                t4.setText(String.valueOf(qnt));
                t5.setText(String.valueOf(qnt * get.getPrice()));
                Double a = Double.parseDouble(prix_total.getText());
                prix_total.setText(String.valueOf(a + get.getPrice()));

                get.setQuantite(qnt);
                AfficheProduitFrontController.nombreproduits++;
            });

            /**
             * **********************************
             */
            hb1.getChildren().addAll(delete);
            hb1_1.getChildren().addAll(image);
            hb1.setStyle("-fx-padding : 15 0 0 15");
            hb1_1.setStyle("-fx-padding : 5 0 0 15");
            hb2.getChildren().add(t2);
            hb2.setStyle("-fx-padding : 15 0 0 15");
            hb3.getChildren().add(t3);
            hb3.setStyle("-fx-padding : 15 0 0 15");
            hb4.getChildren().addAll(moin, t4, plus);
            hb4.setStyle("-fx-padding : 15 0 0 0");
            hb5.getChildren().add(t5);
            hb5.setStyle("-fx-padding : 15 0 0 15");
            group.getChildren().addAll(hb1, hb1_1, hb2, hb3, hb4, hb5);
            Separator sep = new Separator(Orientation.HORIZONTAL);
            sep.setLayoutX(0);
            sep.setLayoutY(144 + k);
            sep.setPrefWidth(sep.getWidth() + 690);
            Separator sep2 = new Separator(Orientation.VERTICAL);
            sep2.setLayoutX(0);
            sep2.setLayoutY(66 + k);
            sep2.setPrefHeight(sep2.getHeight() + 75);
            Separator sep1 = new Separator(Orientation.VERTICAL);
            sep1.setLayoutX(693);
            sep1.setLayoutY(66 + k);
            sep1.setPrefHeight(sep1.getHeight() + 75);
            //pane.getChildren().addAll(hb1,hb2,hb3,hb4,hb5,sep);
            pane.getChildren().addAll(group, sep, sep1, sep2);
            k = k + 80;
            i++;
        }
        panier = (ObservableList<Product>) achat;
        System.out.println("achat " + achat);

        Product.Panier = panier;

        //return panier;
    }

    @FXML
    private void validationCommande(ActionEvent event) throws IOException, SQLException {
    System.out.println("COMMANDE "+Panier);
        ObservableList<Product> commande = Product.Panier;
                ObservableList<Product> commande2 = null;

        for (Product produit : commande) {
             
            produit.setStock(produit.getStock() - produit.getQuantite());
            service_pr.UpdateProduitStock(produit);
            Commande c=new Commande();
            Double prixt = Double.parseDouble(prix_total.getText());
            User u=new User();
            service_commande.AjoutCommandeproduit(c.getId(),produit.getId());

               java.util.Date date = new java.util.Date();
            java.sql.Date dateSql = new java.sql.Date(date.getTime());
         
                          u.setId(1);
            Pdf.TotaleCommande=prixt;
           Pdf.listpanier.addAll(commande);
        Pdf pdf = new Pdf();
        pdf.createpdf(u);
            //service_commande.getCommandeRowLists(c.getId());
           c.setUser(u);
            c.setPrixtotale(prixt);
            c.setValide("en cours");
            c.setDatecom(dateSql);
           System.out.println("hani lena choufni "+user);
            service_commande.AjoutCommande(c);          
            ConcatPDFFiles concatPdf = new ConcatPDFFiles();
                     concatPdf.concat();
        
        }

        ObservableList<Product> pa = FXCollections.observableArrayList();
        cart_table(pa);
        Product.Panier = commande2;
        System.out.println("commande after validation "+Panier);
        AfficheProduitFrontController.nombreproduits = 0;
        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficheProduitFront.fxml"))));

    }

   

}
