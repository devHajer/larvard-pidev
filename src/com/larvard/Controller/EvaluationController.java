/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Evaluation;
import com.larvard.Service.EvaluationService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Abn
 */
public class EvaluationController implements Initializable {

    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private TableView<Evaluation> tableview;
    @FXML
    private TableColumn<Evaluation, Integer> idev;
    @FXML
    private TableColumn<Evaluation, Integer> nomlec;
    @FXML
    private TableColumn<Evaluation, Integer> nomens;
    @FXML
    private TableColumn<Evaluation, String> appric;
    @FXML
    private TableColumn<Evaluation, Integer> decision;

    EvaluationService es ;
    @FXML
    private TableColumn<Evaluation, Integer> enfant;
    @FXML
    private JFXTextField tf_recherche;
    @FXML
    private HBox ActionBox;
    
    public static Evaluation evaluation ;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        es = new EvaluationService();
        
        idev.setCellValueFactory(new PropertyValueFactory<>("id"));
        nomlec.setCellValueFactory(new PropertyValueFactory<>("idepreuve"));
        nomens.setCellValueFactory(new PropertyValueFactory<>("iduser"));
        appric.setCellValueFactory(new PropertyValueFactory<>("appreciation"));
        decision.setCellValueFactory(new PropertyValueFactory<>("decision"));
        enfant.setCellValueFactory(new PropertyValueFactory<>("idenfant"));
        tableview.getItems().setAll(es.getAllEvaluations());
        ActionBox.setDisable(true);
                
        tableview.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<Evaluation>() {
            @Override
            public void changed(ObservableValue observable, Evaluation oldValue, Evaluation newValue) {
                   if(tableview.getSelectionModel().getSelectedItem() != null )
                     ActionBox.setDisable(false);
                   else ActionBox.setDisable(true);
            }
   
     });
        
    }    

    @FXML
    private void rechercheAction(KeyEvent event) {
         System.out.println(tf_recherche.getText());
        tableview.getItems().setAll(es.getEpreuveByAll(tf_recherche.getText()));
    }

    @FXML
    private void modifier() {
        evaluation = new Evaluation();
        evaluation = tableview.getSelectionModel().getSelectedItem();
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/ModifierEvaluation.fxml"));
        Stage stage = (Stage) ActionBox.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void supprimer() {
        es.deleteEvaluation(tableview.getSelectionModel().getSelectedItem().getId());
         tableview.getItems().setAll(es.getAllEvaluations());
//        AlertMaker.sendNotificationApi("Evaluation supprimée avec succées", "");

    }

    @FXML
    private void Ajouter() {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/AjouterEvaluation.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEpreuveBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEvaluationBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EvaluationList.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    
}
