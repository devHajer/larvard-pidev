/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.Entity.Enfant;
import com.larvard.Entity.Evaluation;
// import com.larvard.Entity.Parent;
import com.larvard.Entity.User;
import com.larvard.Service.EpreuveService;
import com.larvard.Service.EvaluationService;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Abn
 */
public class PrincipaleEnafantListController implements Initializable {

    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private TableView<Enfant> tableView;
    @FXML
    private TableColumn<Enfant, String> Nom;
    @FXML
    private TableColumn<Enfant, String> Prenom;
    @FXML
    private TableColumn<Enfant, Date> dateNaissance;

    private EvaluationService es ;
    private EpreuveService eps ;
    
    public static Enfant selectedEnfant ;
    @FXML
    private JFXButton evaBtn;
    @FXML
    private Pane statiquePane;
    @FXML
    private Label reussiCount;
    @FXML
    private Label NonReussiCount;
    @FXML
    private Label ratio;
    @FXML
    private Label nomEleve;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        es = new EvaluationService();
        eps = new EpreuveService();
        
        
        Nom.setCellValueFactory(new PropertyValueFactory<>("Nom"));
        Prenom.setCellValueFactory(new PropertyValueFactory<>("Prenom"));
        dateNaissance.setCellValueFactory(new PropertyValueFactory<>("dateNaissance"));
       
        // To Replace with Get Connected Parent
        User p = new User();
        p = es.getUserById(1);

        tableView.getItems().setAll(es.getEnfantsByUser(p.getId()));
        
        evaBtn.setDisable(true);
        statiquePane.setVisible(false);
         tableView.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<Enfant>() {
            @Override
            public void changed(ObservableValue observable, Enfant oldValue, Enfant newValue) {
                   if(tableView.getSelectionModel().getSelectedItem() != null )
                 {evaBtn.setDisable(false);
                 
                 nomEleve.setText(tableView.getSelectionModel().getSelectedItem().getNom() +" " + tableView.getSelectionModel().getSelectedItem().getPrenom());
                 reussiCount.setText(String.valueOf(es.successTestByEnfant(tableView.getSelectionModel().getSelectedItem().getIDEnfant())));                 
                 NonReussiCount.setText(String.valueOf(es.failTestByEnfant(tableView.getSelectionModel().getSelectedItem().getIDEnfant())));
                 ratio.setText(String.valueOf(es.successRateByEnfant(tableView.getSelectionModel().getSelectedItem().getIDEnfant())));
                 
                 statiquePane.setVisible(true);
                 }else {
                       evaBtn.setDisable(true);
                        statiquePane.setVisible(false);
                   }
            }
   
     });
    }    

    @FXML
    private void listEvaluationBtn() {
        selectedEnfant = tableView.getSelectionModel().getSelectedItem();
         try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PrincipaleEnfantEvaluationsList.fxml"));
        Stage stage = (Stage) lblStatus.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
        
    }
    
        @FXML
    private void AccueilBtn(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PanellFront.fxml"))));             

    }    
    
}
