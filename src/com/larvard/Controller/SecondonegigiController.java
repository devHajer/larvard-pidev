
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;


import com.larvard.Entity.*;
import com.larvard.Service.*;
import java.io.IOException;
import javafx.scene.paint.Color;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import com.larvard.utils.DataBase;
import com.sun.rowset.internal.Row;
import java.util.Optional;
import javafx.collections.ObservableArray;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javax.swing.text.DateFormatter;
import com.larvard.Entity.Activite;
import com.larvard.Entity.AfterSchool;
import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.util.Callback;
import javafx.util.Duration;
import static jdk.nashorn.tools.Shell.SUCCESS;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import static tray.notification.NotificationType.SUCCESS;
import tray.notification.TrayNotification; 
import com.larvard.Service.Pdf;
import java.io.FileOutputStream;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;

import java.io.FileOutputStream; 
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException; 
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.*;
import com.larvard.Entity.Activite;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.util.converter.IntegerStringConverter;

/**
 * FXML Controller class
 *
 * @author bayrem
 */
public class SecondonegigiController implements Initializable {

    ObservableList<String> listType = FXCollections.observableArrayList("sport", "dance", "coloriage", "cuisine junior", "theatre");
    //ServiceActivite seract=new ServiceActivite();

    @FXML
    private Button btnActivit;
    @FXML
    private Button btnConsulterActivit;
    @FXML
    private Button btnAfterschool;
    Boolean verifdate = true;

    @FXML
    private Button btnEnfant;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private GridPane pnajoutActivite;

    @FXML
    private GridPane pnconsulteractivite;
    @FXML
    private GridPane pnafterschool;
    @FXML
    private TextField tftitre;
    @FXML
    private TextArea textareadescription;
    @FXML
    private TextField tfduree;
    @FXML
    private ComboBox combotype;
    @FXML
    private Button btnajout;
    @FXML
    private TableColumn<Activite, String> coidact;
    @FXML
    private TableColumn<Activite, String> cotitre;
    @FXML
    private TableColumn<Activite, String> codescription;
    @FXML
    private TableColumn<Activite, String> cotype;
    @FXML
    private TableColumn<Activite, Integer> coduree;
    ObservableList<Activite> listAct = FXCollections.observableArrayList();
    ObservableList<Enfant> listenf = FXCollections.observableArrayList();

    @FXML
    private TableView<Activite> tabAct;
    
    @FXML
    private Button btnsuppact;
    @FXML
    private ComboBox<String> comboActivite;

    @FXML
    private DatePicker datepic;

    @FXML
    private Button ajoutafs;
    @FXML
    private TableView<AfterSchool> tabAfterschool;
    ObservableList<AfterSchool> listaf = FXCollections.observableArrayList();
    private ObservableList<AfterSchool> data;
    @FXML
    private TableColumn<AfterSchool, Integer> coidaf;
    @FXML
    private TableColumn<AfterSchool, String> codateaf;
    @FXML
    private TableColumn<AfterSchool, String> coact;

    ServiceActivite serA = new ServiceActivite();
    ServiceAfterSchool seraf = new ServiceAfterSchool(); 
    ServiceEnfant serenf = new ServiceEnfant();
    List acti = serA.readTitre();
    List after = seraf.readid();
    ObservableList<Enfant> filterData = FXCollections.observableArrayList();
    ObservableList<Enfant> masterData = FXCollections.observableArrayList(serenf.readnomprdate());
    ObservableList<String> activites = FXCollections.observableArrayList(acti);
    ObservableList<Integer> school = FXCollections.observableArrayList(after);
      Enfant selectedEnfant = new Enfant(); 
      Activite selectedact= new Activite(); 
      AfterSchool selecterafter = new AfterSchool();
    //test search by gigi :
         ObservableList<Activite> ob=FXCollections.observableArrayList();
        FilteredList<Activite> filteredData=new FilteredList<>(ob,e->true);
    @FXML
    private TextField tfrech;
    /////////////////////////////pdf ///////////////////////////
    
    @FXML
    private GridPane pnenfant;
    @FXML
    private TextField tfenfant;
    @FXML
    private ComboBox<Integer> comboaft;
    @FXML
    private Button ajouterlelenfant;
    @FXML
    private TableView<Enfant> tabenf;
    @FXML
    private TableColumn<Enfant, String> colnom;
    @FXML
    private TableColumn<Enfant, String> colprenom;
    @FXML
    private TableColumn<Enfant, String> coldatenai;
    @FXML
    private TableColumn<Enfant,Integer> colfterschool;
    @FXML
    private Button btnmodifact;
    @FXML
    private Button btnsuppenf;
    @FXML
    private Button btnsuppafterschool;

    /**
     * Initializes the controller class.
     */
    
    public SecondonegigiController() {
         filterData.addAll(masterData);
             masterData.addListener(new ListChangeListener<Enfant>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends Enfant> change) {
                updateFilteredData();
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    tabenf.setOnMouseClicked( ev -> {
    if (ev.getClickCount() > 1) {
        onEdit();
    }
       });
    
    
        tabAct.setOnMouseClicked( ev -> {
    if (ev.getClickCount() > 1) {
        onEditact();
    }
       });
        
        
           tabAfterschool.setOnMouseClicked( ev -> {
    if (ev.getClickCount() > 1) {
        onEditafter();
    }
       });
    
                colnom.setCellValueFactory(new PropertyValueFactory<>("nom"));
                colprenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
                coldatenai.setCellValueFactory(new PropertyValueFactory<>("DateNaissance"));
                colfterschool.setCellValueFactory(new PropertyValueFactory<>("IDAfterschool"));
                tabenf.setItems(filterData);

            // Listen for text changes in the filter text field
        tfenfant.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
               
                updateFilteredData();
            }
        });
       
        combotype.setItems(listType);
        comboActivite.setItems(activites);
        comboaft.setItems(school);
        try {
            for (AfterSchool af : seraf.readAll()) {
                listaf.add(af);
                coidaf.setCellValueFactory(new PropertyValueFactory<>("IDafteschool"));
                codateaf.setCellValueFactory(new PropertyValueFactory<>("dateafterschool"));
                coact.setCellValueFactory(p -> new SimpleObjectProperty<>(serA.getTitre(p.getValue().getActivite())));
                tabAfterschool.setItems(listaf);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            //sorted by duree
            for (Activite t : serA.readAll()) {
                listAct.add(t);
                
                coidact.setCellValueFactory(new PropertyValueFactory<>("IDactivite"));
                cotitre.setCellValueFactory(new PropertyValueFactory<>("titre"));
                codescription.setCellValueFactory(new PropertyValueFactory<>("descriptionAct"));
                coduree.setCellValueFactory(new PropertyValueFactory<>("duree"));
                cotype.setCellValueFactory(new PropertyValueFactory<>("type"));
                //loadDatabaseData();
                //loadAct();
                tabAct.setItems(listAct);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
       /* try {
            //sorted by duree : 
            for (Enfant e: serenf.readnomprdate()) {
                listenf.add(e);
                
                colnom.setCellValueFactory(new PropertyValueFactory<>("nom"));
                colprenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
                coldatenai.setCellValueFactory(new PropertyValueFactory<>("DateNaissance"));
                colfterschool.setCellValueFactory(new PropertyValueFactory<>("IDAfterschool"));
                
                tabenf.setItems(listenf);
            }
        } catch (SQLException ex) {        
            System.out.println(ex);
        }*/
    }

    @FXML
    private void handleClick(ActionEvent event) throws SQLException {
        if (event.getSource() == btnActivit) {
            lblStatusMini.setText("/Home/Ajouter Activité");
            lblStatus.setText("Ajouter Activité");
            pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(63, 43, 99), CornerRadii.EMPTY, Insets.EMPTY)));
            pnajoutActivite.toFront();
        } else if (event.getSource() == btnConsulterActivit) {
            lblStatusMini.setText("/Home/Consulter Activité");
            lblStatus.setText("Consulter Activité");
            pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(43, 99, 63), CornerRadii.EMPTY, Insets.EMPTY)));
            pnconsulteractivite.toFront();
           
            

        } else if (event.getSource() == btnAfterschool) {
            lblStatusMini.setText("/Home/ Afterschool");
            lblStatus.setText(" Afterschool");
            pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(99, 43, 63), CornerRadii.EMPTY, Insets.EMPTY)));
            pnafterschool.toFront();

        } else if (event.getSource() == btnEnfant) {
            lblStatusMini.setText("/Home/Enfant");
            lblStatus.setText("Enfant");
            pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(157, 151, 166), CornerRadii.EMPTY, Insets.EMPTY)));
            pnenfant.toFront();
        }

    }

    @FXML
    private void ajouteract(ActionEvent event) throws SQLException, FileNotFoundException, DocumentException { 
        Document doc = new Document();
       PdfWriter.getInstance(doc, new FileOutputStream("activite.pdf"));
        String titre = tftitre.getText();
        String desc = textareadescription.getText();
        int duree = Integer.parseInt(tfduree.getText());
        String type = combotype.getValue().toString(); 
            Activite act = new Activite(titre, desc, duree, type);
  serA.ajouter(act);
  doc.open();
           doc.add(new Paragraph("*******titre activité     " + tftitre.getText() + " ******"));
           doc.add(new Paragraph("****************durée      " + tfduree.getText() + "*******"));
           doc.add(new Paragraph("Description                 " + textareadescription.getText() + "*******"));
           doc.add(new Paragraph("*****************type        " + combotype.getValue().toString() + "*******"));
           doc.close();
   //loadDatabaseData(); 
   
  

                 }
    
     public void loadAct() {

        try {
            List<Activite> activs = serA.readAll();
        } catch (SQLException ex) {
            Logger.getLogger(SecondonegigiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObservableList observableList = FXCollections.observableArrayList(tabAct);
        tabAct.setItems(observableList);

    }

    @FXML
    private void supprimerAct(ActionEvent event) throws SQLException {

      int x=selectedact.getIdActivite();//mochkel el requete 
      serA.delete(x); 
      loadDatabaseData();

}

    @FXML
    private void ajouterAfterSchool(ActionEvent event) throws SQLException, ParseException {

        java.sql.Date d = java.sql.Date.valueOf(datepic.getValue()); 
        int aa = serA.searchactbytitle(comboActivite.getValue());
        AfterSchool af = new AfterSchool(aa, d);
        seraf.ajouter(af); 
        
        
    }

    @FXML
    private void search(ActionEvent event) {
    
    }

     public void loadDatabaseData()
    {          
                 try {
                for (Activite t : serA.readAll()) {
                    ob.add(t);               
                SortedList<Activite> sortedData=new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tabAct.comparatorProperty());
        tabAct.setItems(sortedData);
            }} catch (SQLException ex) {
                System.out.println(ex);
            }
    }
  

     
    /**************************************************************************/
    private void updateFilteredData() {
        filterData.clear();
           
        for (Enfant e : masterData) {
            if (matchesFilter(e)) {
                filterData.add(e);
            }
        }
       
        // Must re-sort table after items changed
        reapplyTableSortOrder();
    }
      /**
     * Returns true if enfant matches the current filter. Lower/Upper case
     * is ignored.
     *
     * @param enfant
     * @return
     */
    private boolean matchesFilter(Enfant e) {
        String filterString = tfenfant.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }
       
        String lowerCaseFilterString = filterString.toLowerCase();
       
        if (e.getNom().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (e.getPrenom().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
       
        return false; // Does not match
    }
   
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<Enfant, ?>> sortOrder = new ArrayList<>(tabenf.getSortOrder());
        tabenf.getSortOrder().clear();
        tabenf.getSortOrder().addAll(sortOrder);
    }

    private void onEdit() {
        if (tabenf.getSelectionModel().getSelectedItem() != null) {
        selectedEnfant = tabenf.getSelectionModel().getSelectedItem();
            System.out.println(tabenf.getSelectionModel().getSelectedItem());
    }
    }
    private void onEditact() {
        if (tabAct.getSelectionModel().getSelectedItem() != null) {
        selectedact = tabAct.getSelectionModel().getSelectedItem();
            System.out.println(tabAct.getSelectionModel().getSelectedItem());
    }}
     private void onEditafter() {
        if (tabAfterschool.getSelectionModel().getSelectedItem() != null) {
        selecterafter = tabAfterschool.getSelectionModel().getSelectedItem();
            System.out.println(tabAfterschool.getSelectionModel().getSelectedItem());
    }}

    @FXML
    private void ajouteravancee(ActionEvent event) {
        
       int idA = comboaft.getValue();
       int idE= selectedEnfant.getIDEnfant();
        System.out.println("Nom Enfant : "+selectedEnfant);
       serenf.updateAfterSchool(idE, idA);
     
        //System.out.println("idC : "+idC+"/"+"idE : "+idE)
    }

    @FXML
    private void supprimerenfant(ActionEvent event) throws SQLException { 
        int i = selectedEnfant.getIDEnfant();
        serenf.delete(i);
    }

    @FXML
    private void supprimerafterschool(ActionEvent event) throws SQLException { 
        int i = selecterafter.getIDAfterschool();
        seraf.delete(i);
    }

}
