/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.Entity.Commande;
import com.larvard.Service.ServiceCommande;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class AfficherCommande implements Initializable {

    @FXML
    private TableView<Commande> tabcmd;
    private TableColumn<Commande, String> tid;
    @FXML
    private TableColumn<Commande, String> tdate;
    @FXML
    private TableColumn<Commande, String> tprix;
    private TableColumn<Commande, String> tclient;
    @FXML
    private TableColumn<Commande, String> tproduit;
    @FXML
    private TableColumn<Commande, String> tvalide;
    /**
     * Initializes the controller class.
     */
    
      ServiceCommande service_cmd=new ServiceCommande();
     private Connection con;
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private JFXButton Panier;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private TableColumn<?, ?> username;
    @FXML
    private JFXButton Produits1;


    public AfficherCommande() {
        con = DataBase.getInstance().getConnection();

    }
    ObservableList<Commande> list =FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            
            for(Commande cmd:service_cmd.readAllCommande())
            {
                list.add(cmd);
                
            }
                   // tid.setCellValueFactory(new PropertyValueFactory<>(""));
                   tdate.setCellValueFactory(new PropertyValueFactory<>("datecom"));
                    
                    tprix.setCellValueFactory(new PropertyValueFactory<>("prixtotale"));
                   // tclient.setCellValueFactory(new PropertyValueFactory<>("user"));
                    tproduit.setCellValueFactory(new PropertyValueFactory<>("produit"));
                    tvalide.setCellValueFactory(new PropertyValueFactory<>("valide"));
                     } catch (SQLException ex) {
                         System.out.println(ex);
        } 
                                                
        TableColumn col_action = new TableColumn<>("valider commande");
        tabcmd.getColumns().add(col_action);
        
        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, 
                ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

    
                    
             @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new ButtonValliderCommande();
            }
        
        });                              
       TableColumn col_action2 = new TableColumn<>("supprimer");
        tabcmd.getColumns().add(col_action2);
        
        col_action2.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, 
                ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_action2.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

    
                    
             @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new ButtonDeleteCommande();
            }
        
        }); 
                            tabcmd.setItems(list);

 
    }   

    @FXML
    private void buttonaccuiel(ActionEvent event) throws IOException {
           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));              

    }

    @FXML
    private void buttonProduit(ActionEvent event) throws IOException {
                    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));             

    }

    @FXML
    private void buttonrating(ActionEvent event) throws IOException {
                    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));             

    }
   
}
