/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

// import com.larvard.Entity.Enseignant;
import com.larvard.Entity.Epreuve;
import com.larvard.Entity.Evaluation;
import com.larvard.Service.EpreuveService;
import com.larvard.Service.EvaluationService;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Abn
 */
public class PrincipaleEnafantEvaluationsListController implements Initializable {

    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private Label eleveLabel;
    @FXML
    private TableView<Evaluation> tableView;
//   @FXML
//    private TableColumn<Evaluation, Enseignant> ens;
    @FXML
    private TableColumn<Evaluation, Integer> decision;
    @FXML
    private TableColumn<Evaluation, String> apprec;
    @FXML
//    private TableColumn<Evaluation, Epreuve> epreuve;
    private TableColumn<Evaluation, Integer> epreuve;
    
    private EvaluationService es ;
    private EpreuveService eps ;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        es = new EvaluationService();
        eps = new EpreuveService();
        
//        ens.setCellValueFactory(new PropertyValueFactory<>("user"));
        decision.setCellValueFactory(new PropertyValueFactory<>("decision"));
        apprec.setCellValueFactory(new PropertyValueFactory<>("appreciation"));
        epreuve.setCellValueFactory(new PropertyValueFactory<>("idepreuve"));
         
         tableView.getItems().setAll(es.getEvaluationByEnfant(PrincipaleEnafantListController.selectedEnfant.getIDEnfant()));
         
         eleveLabel.setText(PrincipaleEnafantListController.selectedEnfant.getNom() + " " + PrincipaleEnafantListController.selectedEnfant.getPrenom());
    }    

    @FXML
    private void listEnfantBtn() {
         try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PrincipaleEnfantList.fxml"));
        Stage stage = (Stage) eleveLabel.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }
    
        @FXML
    private void AccueilBtn(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PanellFront.fxml"))));             

    }      
    
}
