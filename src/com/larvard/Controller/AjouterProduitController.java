/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Product;
import com.larvard.Service.ServiceProduct;
import com.larvard.test.LarvardPI;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class AjouterProduitController implements Initializable {

    ServiceProduct service_pr = new ServiceProduct();
    ObservableList<String> data = FXCollections.observableArrayList("vêtements", "chaussures", "sous-vêtements", "pyjamas", "jouets", "lits", "bureaux", "bibliothéques");
    final FileChooser fileChooser = new FileChooser();
    private Desktop desktop = Desktop.getDesktop();
    private String file_image;
    @FXML
    private Button ajout;
    @FXML
    private ToggleGroup genre;
    @FXML
    private ImageView image_p;
    @FXML
    private JFXTextField prix_pr;
    @FXML
    private JFXTextField age_pr;
    @FXML
    private JFXTextField stock_pr;
    @FXML
    private JFXRadioButton homme;
    @FXML
    private JFXRadioButton femme;
    @FXML
    private JFXComboBox<String> categorie_pr;
    @FXML
    private JFXButton fichier;
    @FXML
    private JFXTextField tnom;
    private Path pathfrom;
    private Path pathto;
    private File Current_file;
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private JFXButton Panier;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private JFXButton Commande;
    @FXML
    private JFXButton Statistique;
    @FXML
    private JFXTextArea description;
    @FXML
    private JFXTextField quant_pr;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        categorie_pr.setItems(data);
    }

    @FXML
    private void ajout(ActionEvent event) throws IOException, SQLException {

        Product p = new Product();
        p.setNom(tnom.getText());
        p.setStock(Integer.parseInt(stock_pr.getText()));
        p.setPrice(Double.parseDouble(prix_pr.getText()));
        p.setDescription(description.getText());
        p.setGenre(radioSelected(event));
        p.setAge(Integer.parseInt(age_pr.getText()));
        p.setCategorie(categorie_pr.getValue());
        p.setQuantite(Integer.parseInt(quant_pr.getText()));
        file_image = "/images/" + file_image;
        p.setImg(file_image);
 service_pr.AjoutProduit(p);

        pathfrom = FileSystems.getDefault().getPath(Current_file.getPath());
        pathto = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\imgproj\\images\\" + Current_file.getName());
        Path targetDir = FileSystems.getDefault().getPath("C:\\xampp\\htdocs\\imgproj\\images\\");

        Files.copy(pathfrom, pathto, StandardCopyOption.REPLACE_EXISTING);

       
        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));
    }

    @FXML
    private String radioSelected(ActionEvent event) {
        String genre = "";
        if (homme.isSelected()) {
            genre += homme.getText();
        }
        if (femme.isSelected()) {
            genre += femme.getText();
        }
        return genre;
    }

    @FXML
    private void fichier_image(ActionEvent event) throws MalformedURLException {
        Current_file = fileChooser.showOpenDialog(LarvardPI.getInstance().getStage());
        if (Current_file != null) {
//                        openFile(file);
            System.out.println(Current_file.toURI().toURL().toExternalForm());
            file_image = Current_file.getName();
        }

        Image image2 = new Image(Current_file.toURI().toString());

        image_p.setImage(image2);

        final GridPane inputGridPane = new GridPane();
        GridPane.setConstraints(fichier, 0, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
    }

    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {

           Logger.getLogger(
                AjouterProduitController.class.getName()).log(
             Level.SEVERE, null, ex
            );
        }
    }

    @FXML
    private void buttonaccuiel(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));              

    }

    @FXML
    private void buttonProduit(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void buttonrating(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void ButtonCommande(ActionEvent event) throws IOException {
                                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));              

    }

    @FXML
    private void ButtonStatistique(ActionEvent event) throws  IOException {
                                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/stat.fxml"))));              

    }

}
