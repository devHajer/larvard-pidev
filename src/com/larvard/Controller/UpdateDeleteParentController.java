/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.User;
import com.larvard.Service.UserService;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author ivaxs
 */
public class UpdateDeleteParentController implements Initializable {

    @FXML
    private Button btnaccueil;
    @FXML
    private Button btnconsultenfant;
    @FXML
    private Button btnconsultenseignant;
    @FXML
    private Pane pane;
    @FXML
    private Label labelpath;
    @FXML
    private Label labeltitle;
    @FXML
    private JFXTextField tfidparent;
    @FXML
    private Label tfNom;
    @FXML
    private JFXTextField tfadresse;
    @FXML
    private JFXTextField tfprenom;
    @FXML
    private JFXTextField tfemail;
    @FXML
    private JFXTextField tfnumtel;
    @FXML
    private JFXTextField tfnom;
    @FXML
    private JFXDatePicker jfxdate;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Connection con;

        // public UpdateDeleteParentController() {
    }

    @FXML
    private void consultenfant(ActionEvent event) {
    }

    @FXML
    private void consultenseignant(ActionEvent event) {
    }

    @FXML
    private void btDelete(ActionEvent event) throws SQLException {
        int id = Integer.parseInt(tfidparent.getText());
        UserService us = new UserService();
        us.delete(id);

    }

    @FXML
    private void btUpdate(ActionEvent event) throws SQLException {
        int id = Integer.parseInt(tfidparent.getText());
        UserService us = new UserService();
        String txtnom = tfNom.getText();
        String txtprenom = tfprenom.getText();
        //String txtdate = jfxdate.;
        String txtmail = tfemail.getText();
        String txtteluser = tfnumtel.getText();
        String txtadresse = tfadresse.getText();
        User u = new User();
        u.setNom(txtteluser);

   us.UpdateUser(id);

    }

    @FXML
    private void btFind(ActionEvent event) {
        int id = Integer.parseInt(tfidparent.getText());
        UserService us = new UserService();
        User u = us.finUserById(id);

        tfNom.setText(u.getNom());
        tfprenom.setText(u.getPrenom());
       // tfdatenaiss.text(u.getDateNaissUser());
        tfemail.setText(u.getEmail());
    

    }

    @FXML
    private void jfxdate(ActionEvent event) {
    }

    @FXML
    private void btnaccueil(ActionEvent event) throws IOException {
        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendParent.fxml"))));

    }

}
