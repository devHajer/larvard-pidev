/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.larvard.Entity.User;
import com.larvard.Service.UserService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class ProfilUserController implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private TextField username;
    @FXML
    private TextField num;
    @FXML
    private Button signup;
    @FXML
    private DatePicker date;
    @FXML
    private TextField address;
    @FXML
    private Label labelemail;
    @FXML
    private Label labelusername;
    @FXML
    private Label labelNum;
    @FXML
    private Label labeldate;
    @FXML
    private ImageView emailTick;
    @FXML
    private ImageView usernameTick;
    @FXML
    private ImageView mdpTick;
    @FXML
    private ImageView mdp2Tick;
    @FXML
    private ImageView numeroTick;
    @FXML
    private ImageView dateTick;
    @FXML
    private PasswordField mdp;
    @FXML
    private PasswordField cmdp;
    @FXML
    private Label labelcmdp;
private User user;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UserService ser=new UserService();
        
        User u=ser.finUserById(user.getId());
        nom.setText(u.getNom());
        prenom.setText(u.getPrenom());
        email.setText(u.getEmail());
     
                
                
    }    

 

    
}
