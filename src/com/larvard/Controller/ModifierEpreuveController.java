/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Epreuve;
import com.larvard.Service.EpreuveService;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Abn
 */
public class ModifierEpreuveController implements Initializable {

    @FXML
    private JFXTextField tf_ref;
    @FXML
    private JFXDatePicker dp_dateDebut;
    @FXML
    private JFXComboBox<Integer> cb_niveau;
    @FXML
    private JFXSlider s_duree;
    @FXML
    private JFXComboBox<String> cb_type;
    
    
    private EpreuveService es ;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        es = new EpreuveService();
        // Preparing Nivaux ComboBox List
        ArrayList<Integer> niveaux = new ArrayList<Integer>();
        niveaux.add(1);
        niveaux.add(2);
        niveaux.add(3);
        niveaux.add(4);
        cb_niveau.getItems().addAll(niveaux);
        // Preparing Type ComboBox List
        ArrayList<String> types = new ArrayList<String>();
        types.add("Orale");
        types.add("Ecrit");
        types.add("Autres");
        cb_type.getItems().addAll(types);
        Epreuve e = EpreuveFXMLController.epreuve ;
        
        cb_niveau.getSelectionModel().select(e.getNiveau());
        cb_type.getSelectionModel().select(e.getType());
        tf_ref.setText(e.getReference());
        
        dp_dateDebut.setValue(LocalDate.now());
        
        
        
        
    }    

    @FXML
    private void Modifier(ActionEvent event) {
        Epreuve e = EpreuveFXMLController.epreuve;
        String pattern = "MM/dd/yyyy";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
        try {
            if(!tf_ref.getText().isEmpty())
                e.setReference(tf_ref.getText());
            else 
            {
//                AlertMaker.showErrorMessage("Données Manquante", "Le champ de la réference est vide!");
                return ;
            }           
            if(! dp_dateDebut.getValue().format(dateFormatter).isEmpty() )
            {   System.out.println(dp_dateDebut.getValue().format(dateFormatter));
                e.setDateEp(dp_dateDebut.getValue().format(dateFormatter));
            }
            else 
            {
//                AlertMaker.showErrorMessage("Données Manquante", "Le champ de la date est vide!");
                return ;
            }
             if(cb_niveau.getSelectionModel().getSelectedItem() != null)
                e.setNiveau(cb_niveau.getSelectionModel().getSelectedItem());
            else 
            {
//                AlertMaker.showErrorMessage("Données Manquante", "Le champ de la niveau est vide!");
                return ;
            }
              if(!cb_type.getSelectionModel().getSelectedItem().isEmpty())
                e.setType(cb_type.getSelectionModel().getSelectedItem());
            else 
            {
//                AlertMaker.showErrorMessage("Données Manquante", "Le champ de la réference est vide!");
                return ;
            }
              e.setDuree((int)s_duree.getValue());
              es.updateEp(e);
//              AlertMaker.sendNotificationApi("Epreuve Modifié avec succées", "");
              
                
        }
        catch( Exception ex )
        {
//             AlertMaker.showErrorMessage(ex);
        }
    }

    @FXML
    private void Annuler() {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"));
        Stage stage = (Stage) tf_ref.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
        
    }

      @FXML
    private void GestionEpreuveBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"));
        Stage stage = (Stage) tf_ref.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEvaluationBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EvaluationList.fxml"));
        Stage stage = (Stage) tf_ref.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }
    
}
