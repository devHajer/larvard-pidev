/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.larvard.Entity.Product;
import static com.larvard.Entity.Product.getPanier;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class PanierController implements Initializable {

    TextField txt = new TextField();
    Text total = new Text();
    Product p = new Product();
    @FXML
    private JFXDrawer drawer;
    @FXML
    private JFXHamburger hamburger;
//    @FXML
//    private TableColumn<Produit, Integer> id_p;
    @FXML
    private TableColumn<Product, String> nom_p;
    @FXML
    private TableColumn<Product, String> image_p;
    @FXML
    private TableView<Product> table_view;
    @FXML
    private TableColumn<Product, Integer> quantite_p;
    @FXML
    private TableColumn<Product, Integer> prix_p;
    @FXML
    private TableColumn<Product, Integer> prixtotal_p;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     
        ObservableList<Product> panier = FXCollections.observableArrayList();
        for (Product p : getPanier()) {
            panier.add(p);
            System.out.println(panier);
        }
        int prixtotal = 0;
        int prixtotal_cell = 0;
        for (int i = 0; i < panier.size(); i++) {
            Product get = panier.get(i);
            prixtotal = (int) (prixtotal + (get.getQuantite() * get.getPrice()));
            prixtotal_cell = (int) (get.getQuantite() * get.getPrice());
        }
        System.out.println("prix total " + prixtotal);
        nom_p.setCellValueFactory(new PropertyValueFactory<>("Nom"));
        image_p.setCellValueFactory(new PropertyValueFactory<>("img"));
        quantite_p.setCellValueFactory(new PropertyValueFactory<>("quantite"));
        prix_p.setCellValueFactory(new PropertyValueFactory<>("price"));
        prixtotal_p.setCellValueFactory(new PropertyValueFactory<>("prixTotale"));

        TableColumn col_action = new TableColumn<>("prit total");
        table_view.getColumns().add(col_action);

        prixtotal_p.setCellFactory((TableColumn<Product, Integer> param) -> {
            return new TableCell<Product, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                    }
                    if (item != null) {
                        String b = quantite_p.getText();


                        total.setText(txt.getText());

                        setGraphic(total);
                    }
                }
            };
        });

        image_p.setCellFactory((TableColumn<Product, String> param) -> {
            return new TableCell<Product, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                    }
                    if (item != null) {
                        String A = item;
                        A = "C:\\xampp\\htdocs\\medproj\\" + A;
                        File F1 = new File(A);
                        Image image2 = new Image(F1.toURI().toString());

                        ImageView imageView = new ImageView(image2);
                        imageView.setFitHeight(50);
                        imageView.setFitWidth(50);
                        setGraphic(imageView);
                    }
                }
            };
        });

        quantite_p.setCellFactory((TableColumn<Product, Integer> param) -> {
            return new TableCell<Product, Integer>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                    }
                    if (item != null) {
                        Button cellButton2 = new Button("+");
                        Button cellButton = new Button("-");

                        final Pane p = new Pane();
                        final HBox hb = new HBox();

                        txt.setText(String.valueOf(item));
                        if (item > 0) {
                            cellButton.setOnMouseClicked((MouseEvent event) -> {
                                int a = item;

                                a = a - 1;
                                txt.setText(String.valueOf(a));

                                updateItem(a, empty);

                            });
                        }
                        cellButton2.setOnMouseClicked((MouseEvent event) -> {
                            int a = item;
                            a = a + 1;
                            txt.setText(String.valueOf(a));

                            updateItem(a, empty);
                        });
                        hb.setLayoutX(0);
                        hb.setLayoutY(12);
                        hb.getChildren().addAll(cellButton, txt, cellButton2);
                        p.getChildren().add(hb);

                        setGraphic(p);
                    }
                }
            };
        });

  
        table_view.setItems(panier);

    }

}
