/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class PannellAdminController implements Initializable {

    @FXML
    private JFXButton Evenment;
    @FXML
    private JFXButton Produits;
    @FXML
    private JFXButton Activite;
    @FXML
    private JFXButton Afterschool;
    @FXML
    private JFXButton Users;
    @FXML
    private JFXButton Classe;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Hyperlink log;
    @FXML
    private ImageView image;
    @FXML
    private Hyperlink Profil;
    @FXML
    private AnchorPane anchor;
    @FXML
    private JFXButton GestionElearning;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    /**
     * @return the anchor
     */
    public AnchorPane getAnchor() {
        return anchor;
    }

    /**
     * @param anchor the anchor to set
     */
    public void setAnchor(AnchorPane anchor) {
        this.anchor = anchor;
    }
    @FXML
    private void buttonEvenment(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/Main.fxml"))));             

    }

    @FXML
    private void buttonProduit(ActionEvent event) throws IOException {
        
             LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));             

        
    }

    @FXML
    private void buttonActivite(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/secondonegigi.fxml"))));             

    }

    @FXML
    private void ButtonAfterschool(ActionEvent event) {
    }

    @FXML
    private void ButtonUsers(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendEnseignant.fxml"))));             

    }

    @FXML
    private void ButtonClasse(ActionEvent event) {
    }


    @FXML
    private void Logout(ActionEvent event) throws IOException {
             LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/SignIn.fxml"))));             

    }

    @FXML
    private void Profil(ActionEvent event) throws IOException {
    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/ProfilUser.fxml"))));             

    }

    @FXML
    private void GestionElearning(ActionEvent event) throws IOException {
        
            LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"))));             

    }
    
}
