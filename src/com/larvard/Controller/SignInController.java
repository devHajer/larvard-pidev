/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.larvard.Entity.User;
import com.larvard.Service.Password;
import com.larvard.Service.UserService;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class SignInController implements Initializable {

    public static int userIden = 0;

    @FXML
    private TextField username;
    @FXML
    private Button connecter;
    @FXML
    private Hyperlink inscri;
    @FXML
    private PasswordField mdp;
    @FXML
    private AnchorPane anchor = new AnchorPane();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        username.setVisible(true);
        mdp.setVisible(true);
    }

    @FXML
    private void connecter(ActionEvent event) throws IOException {

        try {

            UserService us = new UserService();
            User user = new User();
            user = us.connecter(username.getText());
            System.out.println(user.getRoles());

            Password md = new Password();
            Boolean mdpCrypte = md.checkPassword(mdp.getText(),user.getPassword());

            System.out.println(mdpCrypte);

            userIden = user.getId();
            System.out.println("hello " + userIden);
            if (mdpCrypte == true) {

                System.out.println("authentification reussite");

                if (user.getRoles().equals("a:2:{i:1;s:10:\"ROLE_ADMIN\";}")) {

                    //System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"));

                    Parent root = loader.load();

                    // PannelAdminController pannelAdmin = loader.getController();
                    FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"));

                    Parent root2 = loader2.load();
                    ScrollPane sp = new ScrollPane();
//                    sp.setContent(root2);
//                    sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
//                    sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
//                    root.resize(1080, 720);
                    //AccueilAdminController ad = loader2.getController();

                    //pannelAdmin.getContainer_admin().getChildren().setAll(root2);
                    mdp.getScene().setRoot(root);

                } else if ((user.getRoles().equals("a:2:{i:1;s:10:\"ROLE_ADMIN\";}") )) {
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"));
                        Parent root = loader.load();
                        mdp.getScene().setRoot(root);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //PannelUserController pannelUser = loader.getController();
                    //  pannelUser.setIduser(user.getId());
                    //primaryStage.setMaximized(true);a:1:{i:0;s:11:"ROLE_PARENT";}
                } else if (user.getRoles().equals("a:1:{i:0;s:11:\"ROLE_PARENT\";}")) {
                    System.out.println("haaaaaaaaaani leeeeeeeeeeennna");
                    System.out.println("haaaaaaaaaani leeeeeeeeeeennna");
                    /*                                                                  System.out.println("haaaaaaaaaani leeeeeeeeeeennna");
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PanellFront.fxml"));
                    Parent root = loader.load();
                    ReservationController pannelUser = loader.getController();
                    System.out.println(pannelUser);
                    pannelUser.setUser(user);
                   // pannelUser.initializeP();
                    System.out.println(loader);
                    System.out.println(pannelUser.getUser().getUsername());
                    loader.setController(pannelUser);
                    mdp.getScene().setRoot(root);*/
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/PanellFront.fxml"));
                        Parent root = loader.load();
                        PanellFrontController pannelUser = loader.getController();
                        pannelUser.setUser(user);
                        System.out.println(pannelUser);
                        loader.setController(pannelUser);
                        mdp.getScene().setRoot(root);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setHeaderText("Warning");
                alert.setContentText("veuillez verifier votre login ou mot de passe ..");
                alert.show();
                System.out.println("veuillez verifier votre login ou mot de passe ..");
            }

        } catch (SQLException ex) {

        }

    }

    @FXML
    private void inscription(ActionEvent event) throws IOException {

        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/signup.fxml"))));

    }

}
