
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | com.Larvard.LarvardPI
 * and open the template in the editor.
 */
package com.larvard.Controller;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Product;
import com.larvard.Entity.User;
import com.larvard.Service.ServiceProduct;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;


/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class UpdateProduitController implements Initializable {

        ServiceProduct service_pr=new ServiceProduct();
    ObservableList<String> data = FXCollections.observableArrayList("vêtements", "chaussures","sous-vêtements", "pyjamas","jouets","lits","bureaux","bibliothéques");
    
        Product P=new Product();
        @FXML
    private Button update;
    @FXML
    private JFXTextField nom_pr;
    @FXML
    private JFXTextField prix_pr;
    @FXML
    private JFXTextField age_pr;
    @FXML
    private JFXTextField stock_pr;
    @FXML
    private ImageView image_p;
    @FXML
    private JFXTextArea description_pr;
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private JFXButton Rating;
    @FXML
    private JFXButton Commande;
    @FXML
    private JFXButton Statistique;
    @FXML
    private JFXButton Ajouter;
    @FXML
    private JFXComboBox<String> categorie_pr;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          categorie_pr.setItems(data);

       
        
           Product P;
            try {
                P = service_pr.GetProduitbyid(Product.getId_courant());
         
                   
                     
        nom_pr.setText(String.valueOf(P.getNom()));   
        stock_pr.setText(String.valueOf(P.getStock()));
        prix_pr.setText(String.valueOf(P.getPrice()));
        age_pr.setText(String.valueOf(P.getAge()));
        description_pr.setText(P.getDescription());
            } catch (SQLException ex) {
                System.out.println(ex);
            }
    


}

    @FXML
    private void update(ActionEvent event) throws SQLException {
        P.setId(Product.getId_courant());
        P.setNom(nom_pr.getText());
          P.setCategorie(categorie_pr.getValue());
        P.setStock((int) Double.parseDouble(stock_pr.getText()));
        P.setAge(Integer.parseInt(age_pr.getText()));
        P.setDescription((description_pr.getText()));
             service_pr.updateProduit(P);
            try {              
                LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));
            } catch (IOException ex) {
                Logger.getLogger(UpdateProduitController.class.getName()).log(Level.SEVERE, null, ex);
            }

        
    }

    @FXML
    private void ButtonAccuiel(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));              

    }

    @FXML
    private void ButtonProduit(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void BottonRating(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void ButtonCommande(ActionEvent event) throws IOException {
                           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherCommandeFXML.fxml"))));              

    }

    @FXML
    private void ButtonStaistique(ActionEvent event) throws IOException {
                                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/stat.fxml"))));              

            
    }

    @FXML
    private void ButtonAjouterProduit(ActionEvent event) throws IOException {
                                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));              

    }

}