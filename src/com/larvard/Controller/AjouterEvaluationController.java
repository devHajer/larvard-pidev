/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.larvard.Entity.Enfant;
// import com.larvard.Entity.Enseignant;
import com.larvard.Entity.Epreuve;
import com.larvard.Entity.Evaluation;
import com.larvard.Entity.User;
import com.larvard.Service.EpreuveService;
import com.larvard.Service.EvaluationService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Abn
 */
public class AjouterEvaluationController implements Initializable {

    @FXML
    private JFXTextArea tf_appreciation;
    @FXML
    private JFXComboBox<Enfant> cb_enfants ;
    @FXML
    private JFXComboBox<Epreuve> cb_epreuves;
    @FXML
    private JFXComboBox<String> cb_decision;

    EvaluationService es ;
    EpreuveService eps ;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
        es = new EvaluationService();
        eps = new EpreuveService();
        cb_enfants.getItems().addAll(es.getAllEnfants());
        
            cb_epreuves.getItems().addAll(eps.readAll());
        } catch (SQLException ex) {
         //   AlertMaker.showErrorMessage(ex);
            Logger.getLogger(AjouterEvaluationController.class.getName()).log(Level.SEVERE, null, ex);
        }      
        
        cb_decision.getItems().add("Réussi");cb_decision.getItems().add("Non Réussi");
    }    

    @FXML
    private void Ajouter() {
        Evaluation e = new Evaluation();
        try {
           
            if( cb_decision.getSelectionModel().getSelectedItem()!= null )
                switch(cb_decision.getSelectionModel().getSelectedItem())
                {
                    case "Réussi" : e.setDecision(1); break;
                    case "Non Réussi" : e.setDecision(0); break;
                    default: e.setDecision(0); break;
                }
                
            else 
            {
                //AlertMaker.showErrorMessage("Données Manquante", "Le champ de la decision est vide!");
                return ;
            }
              if(cb_epreuves.getSelectionModel().getSelectedItem() != null)
                e.setIdepreuve(cb_epreuves.getSelectionModel().getSelectedItem().getIdEp());
            else 
            {
                //AlertMaker.showErrorMessage("Données Manquante", "Le champ d'epreuve est vide!");
                return ;
            }
               if(!tf_appreciation.getText().isEmpty())
                e.setAppreciation(tf_appreciation.getText());
            else 
            {
                //AlertMaker.showErrorMessage("Données Manquante", "Le champ de la appreciation est vide!");
                return ;
            }           
            if(cb_enfants.getSelectionModel().getSelectedItem() != null)
            e.setIdenfant(cb_enfants.getSelectionModel().getSelectedItem().getIDEnfant());
            else 
            {
                //AlertMaker.showErrorMessage("Données Manquante", "Le champ d'enfant est vide!");
                return ;
            }            
               // call to static enseignant To be changed by current Connected user
              User en = new User();
              en = es.getUserById(1);
              e.setIduser(en.getId());
              es.addEvaluation(e);

//              MailServerInfo mailServerInfo = new MailServerInfo("smtp.gmail.com", 587, "ecosmartpluus@gmail.com", "espritpidev", Boolean.TRUE);
////              EmailUtil.sendMail(mailServerInfo,"nouha.aloui@esprit.tn", "Evaluation Pour " + e.getEnfant().getNom() + " " + e.getEnfant().getPrenom(), "Votre Enfant a eté évalué");
//              EmailUtil.sendMail(mailServerInfo,"nouha.aloui@esprit.tn", "Evaluation Pour " + e.getIdenfant(), "Votre Enfant a eté évalué");
//              AlertMaker.sendNotificationApi("Evaluation Ajouté avec succées", " U email est envoyé à ");
              
                
        }
        catch( Exception ex )
        {
//             AlertMaker.showErrorMessage(ex);
        }
    }

    @FXML
    private void Annuler(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EvaluationList.fxml"));
        Stage stage = (Stage) tf_appreciation.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
        
    }
    @FXML
    private void GestionEpreuveBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"));
        Stage stage = (Stage) tf_appreciation.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEvaluationBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EvaluationList.fxml"));
        Stage stage = (Stage) tf_appreciation.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }
    
}
