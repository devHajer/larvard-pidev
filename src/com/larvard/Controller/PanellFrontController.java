/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class PanellFrontController implements Initializable {

    private User user;
    @FXML
    private AnchorPane anchor;
    @FXML
    private JFXButton Evenment;
    @FXML
    private JFXButton Shop;
    @FXML
    private JFXButton Activite;
    @FXML
    private JFXButton Afterschool;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton Classe;
    @FXML
    private JFXButton Reclamation;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Hyperlink log;
    @FXML
    private JFXTextField nomuser;
    @FXML
    private ImageView image;

    /**
     * Initializes the controller class.
     */
    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the anchor
     */
    public AnchorPane getAnchor() {
        return anchor;
    }

    /**
     * @param anchor the anchor to set
     */
    public void setAnchor(AnchorPane anchor) {
        this.anchor = anchor;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonEvenment(ActionEvent event) throws IOException {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/Reservation.fxml"));
            Parent root = loader.load();
            anchor.getChildren().setAll(root);
            ReservationController controller = loader.getController();
            System.out.println(user);

            controller.setUser(user);
        } catch (IOException ex) {
            Logger.getLogger(PanellFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void buttonShop(ActionEvent event) {

        try {
         FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/AfficheProduitFront.fxml"));
               Parent root = loader.load();
                      AfficheProduitFrontController controller = loader.getController();
   System.out.println(user);
     //anchor.isResizable();
            controller.setUser(user);
            anchor.getChildren().setAll(root);
     
        } catch (IOException ex) {
            Logger.getLogger(AfficheProduitFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void buttonActivite(ActionEvent event) {
        
    }

    @FXML
    private void ButtonAfterschool(ActionEvent event) {
    }

    @FXML
    private void ButtonProfil(ActionEvent event) {
    }

    @FXML
    private void ButtonClasse(ActionEvent event) {
    }

    @FXML
    private void ButtonReclamation(ActionEvent event) {
    }

    @FXML
    private void Logout(ActionEvent event) {
    }

    @FXML
    private void TextNomUser(ActionEvent event) {
    }

}
