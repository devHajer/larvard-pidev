/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.larvard.Entity.User;
import com.larvard.Service.EnseignantService;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class BackendEnseignantController implements Initializable {
Connection con;


     public BackendEnseignantController() {
        con = DataBase.getInstance().getConnection();

    }
      
                  EnseignantService service_us=new EnseignantService();
                  ObservableList<User> list= FXCollections.observableArrayList();
    @FXML
    private Button btnaccueil;
    @FXML
    private Button btnconsultenseignant;
    @FXML
    private Button btnconsultparent;
    
    @FXML
    private Pane pane;
    @FXML
    private Label labelpath;
    @FXML
    private Label labeltitle;
    
    @FXML
    private TableView<User> tableview;
    @FXML
    private TableColumn<User, String> tnom;
    @FXML
    private TableColumn<User, String> tprenom;
    @FXML
    private TableColumn<User, String> temail;
    @FXML
    private TableColumn<User, String> tadresse;
    @FXML
    private TableColumn<User, String> ttel;
    @FXML
    private TableColumn<User, String> tdatenaiss;

    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
           for (User u:service_us.listEnseignantAdmin())
        {
            list.add(u);
            
        }

      
    tnom.setCellValueFactory(new PropertyValueFactory<>("nomUser"));
        tprenom.setCellValueFactory(new PropertyValueFactory<>("prenomUser"));
         temail.setCellValueFactory(new PropertyValueFactory<>("mailUser"));
        tadresse.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        ttel.setCellValueFactory(new PropertyValueFactory<>("telUser"));
        //tdatenaiss.setCellValueFactory(new PropertyValueFactory<>("datenaissuser"));
        tableview.setItems(list);
      

    }    


    @FXML
    private void consultenseignant(ActionEvent event) throws IOException {
    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendEnseignant.fxml"))));          

    }

    @FXML
    private void consultparent(ActionEvent event) throws IOException {
               LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendParent.fxml"))));          
    
    }

    @FXML
    private void accueil(ActionEvent event) throws IOException {
                       LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));          

    }

    
}
