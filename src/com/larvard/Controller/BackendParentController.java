/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.larvard.Entity.User;
import com.larvard.Service.ParentService;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class BackendParentController implements Initializable {
Connection con;


     public BackendParentController() {
        con = DataBase.getInstance().getConnection();

    }
      
                  ParentService service_us=new ParentService();
                  ObservableList<User> list= FXCollections.observableArrayList();
    @FXML
    private Button btnaccueil;
    @FXML
    private Button btnconsultenfant;
    @FXML
    private Button btnconsultenseignant;
    @FXML
    private Button btnconsultparent;
    
    @FXML
    private Button btnEnfant;
    @FXML
    private Pane pane;
    @FXML
    private Label labelpath;
    @FXML
    private Label labeltitle;
    
    @FXML
    private TableView<User> tableview;
    @FXML
    private TableColumn<User, String> tnom;
    @FXML
    private TableColumn<User, String> tprenom;
    @FXML
    private TableColumn<User, String> temail;
    @FXML
    private TableColumn<User, String> tadresse;
    @FXML
    private TableColumn<User, String> ttel;
    @FXML
    private TableColumn<User, String> tdatenaiss;

    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
           for (User u:service_us.listParentAdmin())
        {
            list.add(u);
            
        }

      
       tnom.setCellValueFactory(new PropertyValueFactory<>("nomUser"));
        tprenom.setCellValueFactory(new PropertyValueFactory<>("prenomUser"));
         temail.setCellValueFactory(new PropertyValueFactory<>("mailUser"));
        tadresse.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        ttel.setCellValueFactory(new PropertyValueFactory<>("telUser"));
        tdatenaiss.setCellValueFactory(new PropertyValueFactory<>("datenaissuser"));
  TableColumn col_action = new TableColumn<>("supprimer");
        tableview.getColumns().add(col_action);
                           
        tableview.setItems(list);

    }    

    @FXML
    private void onhandelclicks(ActionEvent event) {
        
    }

    @FXML
    private void consultenfant(ActionEvent event) {
    }
    

    @FXML
    private void consultenseignant(ActionEvent event) throws IOException {
                                LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendEnseignant.fxml"))));             

    }

    @FXML
    private void consultparent(ActionEvent event) throws IOException {
                                        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendParent.fxml"))));             

    }
    
}
