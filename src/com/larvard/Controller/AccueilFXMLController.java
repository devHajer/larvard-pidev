/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.Entity.User;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class AccueilFXMLController implements Initializable {

    private User user;
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private JFXButton Panier;
    @FXML
    private AnchorPane anchor;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void afficherback(ActionEvent event) throws IOException {
                LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));             

    }

    @FXML
    private void afficherpanier(ActionEvent event) throws IOException {
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/com/larvard/Gui/cartpage.fxml"));                        
            Parent root=loader.load();
            System.out.println("love you"+user);
            CartePageController controller=loader.getController();
            controller.setUser(user);
            anchor.getScene().setRoot(root);
    }

    @FXML
    private void accueilaffiche(ActionEvent event) throws IOException {
                    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficheProduitFront.fxml"))));             

    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the anchor
     */
    public AnchorPane getAnchor() {
        return anchor;
    }

    /**
     * @param anchor the anchor to set
     */
    public void setAnchor(AnchorPane anchor) {
        this.anchor = anchor;
    }
    
}
