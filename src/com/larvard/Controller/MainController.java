/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;
import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.*;
import com.larvard.Service.ServiceEvenement;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author Bechir
 */
public class MainController implements Initializable {
 private Connection con;
    @FXML
    private TableColumn<Evenement, Integer> IDEvt;
    @FXML
    private TableColumn<Evenement, String> Nom;
    @FXML
    private TableColumn<Evenement, String> Lieu;
    @FXML
    private TableColumn<Evenement, String> Date;
    @FXML
    private TableColumn<Evenement, Integer> Participants;
    @FXML
    private TableView<Evenement> tabEvenement;
    private TextField areaLieu;
    @FXML
    private TextField areaNom;
    @FXML
    private DatePicker areaDate;
    @FXML
    private Button btnajouterr;
    @FXML
    private Label areaControle;
    @FXML
    private ComboBox<String> comboLieu;
    @FXML
    private Button Modifier;
    @FXML
    private GridPane onModifier;
    private TextField setLieu;
    @FXML
    private Button btnModifier;
    @FXML
    private Button btnSave;
    @FXML
    private TableView<Evenement> tableUpdt;
    @FXML
    private TableColumn<Evenement, Integer> IDEvts;
    @FXML
    private TableColumn<Evenement, String> Noms;
    @FXML
    private TableColumn<Evenement, String> Lieus;
    @FXML
    private TableColumn<Evenement, String> Dates;
    @FXML
    private TableColumn<Evenement, Integer> Participantss;
    @FXML
    private JFXTextField setLieus;
    @FXML
    private TextField areaRecherche;
   /* @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;*/ 
       public MainController() {
      con = DataBase.getInstance().getConnection();
     
     }
    @FXML
    private Button btnAjout;
    @FXML
    private Button btnAfficher;
    @FXML
    private Button btnReporter;
    @FXML
    private Button btnAjouter;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private GridPane onAjout;
    @FXML
    private GridPane onReporter;
    @FXML
    private GridPane onEvenement;

    public ObservableList<Evenement> data;
   ServiceEvenement seract = new ServiceEvenement() ;
   
   ObservableList<String> dat = FXCollections.observableArrayList("Ariana",
              "Béja", "Ben Arous", "Bizerte", "Monastir");



    /**
     * Initializes the controller class.
     */
   ObservableList<Evenement> list = FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        comboLieu.setItems(dat);
    }    
    @FXML
    private void handleClick(ActionEvent event)
    {
        if(event.getSource() ==btnAjout)
        {
           lblStatusMini.setText("/Home/Ajouter Evenement");
           lblStatus.setText("Evenement");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 63, 43,99),CornerRadii.EMPTY,Insets.EMPTY)));
          onAjout.toFront();
          
        }
        else if(event.getSource() ==btnAfficher)
        {
           lblStatusMini.setText("/Home/Afficher");
           lblStatus.setText("Afficher Evenements");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 43,99,63),CornerRadii.EMPTY,Insets.EMPTY)));
          onEvenement.toFront();
           try {
            data = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"),rs.getInt("nbpart")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDEvt.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("idevent"));
                   Lieu.setCellValueFactory(new PropertyValueFactory<Evenement, String>("local"));
                  Date.setCellValueFactory(new PropertyValueFactory<Evenement, String>("date"));
                    Nom.setCellValueFactory(new PropertyValueFactory<Evenement, String>("nom"));
                    Participants.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("nbpart"));
                    tabEvenement.setItems(data);
                   
        }
         else if(event.getSource() ==btnReporter)
        {
           lblStatusMini.setText("/Home/Reporter");
           lblStatus.setText("Repoter");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 99, 43,63),CornerRadii.EMPTY,Insets.EMPTY)));
          onReporter.toFront();
        }
        
        else if(event.getSource() ==Modifier)
        {
           lblStatusMini.setText("/Home/Modifier");
           lblStatus.setText("Changer lieu");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 99, 43,38),CornerRadii.EMPTY,Insets.EMPTY)));
          onModifier.toFront();
          try {
            data = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"),rs.getInt("nbpart")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDEvts.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("idevent"));
                   Lieus.setCellValueFactory(new PropertyValueFactory<Evenement, String>("local"));
                  Dates.setCellValueFactory(new PropertyValueFactory<Evenement, String>("date"));
                    Noms.setCellValueFactory(new PropertyValueFactory<Evenement, String>("nom"));
                    Participantss.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("nbpart"));
                    tableUpdt.setItems(data);
        }
    
}

    @FXML
    private void supprimer(ActionEvent event) throws SQLException {
        TableViewSelectionModel selectionModel = tabEvenement.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
                  int x=tabEvenement.getItems().get(row).toStringid();
                seract.delete1(x);
//                for ( int i = 0; i<tabEvenement.getItems().size(); i++) {
//    tabEvenement.getItems().clear();
//}
                ObservableList<Evenement> all,single ;
        all=tabEvenement.getItems() ;
        single=tabEvenement.getSelectionModel().getSelectedItems() ;
        // act= new Activite() ;
        //remove=seract.delete(single) ;
        single.forEach(all::remove);
                
        }

    @FXML
    private void ajouter(ActionEvent event) throws SQLException, ParseException, IOException {
        if(controleSaisie()==true) {
        Evenement e = new Evenement();  
           //e.setLieu(areaLieu.getText());
           e.setLieu(comboLieu.getValue());
           e.setNom(areaNom.getText()); 
           //e.setNbpart(0);
           Timestamp dated = Timestamp.valueOf(areaDate.getValue().atTime(LocalTime.MIDNIGHT));
           e.setDatet(dated);
           //java.sql.Date date;
           //date =java.sql.Date.valueOf(areaDate.getValue());
           
           //e.setDates(date);
        seract.add(e);
        
        }
        int edit=000;
    }
 public void refresh(){
     data.clear();
     try {
            data = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"),rs.getInt("nbpart")));
            }
            tableUpdt.setItems(data);
        } catch (SQLException e) {
            e.printStackTrace();

        }
 }
public boolean controleSaisie() throws IOException, SQLException {
        boolean saisie = true;

       
//        if  (areaLieu.getText().equals("")) {
//            areaControle.setText("Empty Text Field!");
//            saisie = false;
//        } else {
//            areaControle.setText("");
//        }
        if (areaNom.getText().equals("")) {
            areaControle.setText("Empty Text Field!");
            saisie = false;
        } else {
            areaControle.setText("");
        }
//        if (quantitytf.getText().equals("")) {
//            quantitelabel.setText("Empty Text Field");
//            saisie = false;
//        } else {
//            quantitelabel.setText("");
//        }
//if (pricetf.getText().equals("")) {
//    pricelabel.setText("Empty Text Field");        
//    saisie = false;
//        } else {
//            pricelabel.setText("");
//        }
//if (promotiontf.getText().equals("")) {
//    namecategorylabe.setText("Empty Text Field");       
//    saisie = false;
//        } else {
//            promotionlabel.setText("");
//        }
       
        return saisie;
    }

    @FXML
    private void updateClick(ActionEvent event) throws SQLException {
        if(event.getSource()==btnModifier){
//            Evenement e = (Evenement) tableUpdt.getSelectionModel().getSelectedItems();
            //setLieu.setText(e.getLieu());
TableViewSelectionModel selectionModel = tableUpdt.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
                  String x=tableUpdt.getItems().get(row).getLieu();
                  //String y=tableUpdt.getItems().get(row).getNom();
                  setLieus.setText(x);
            
        }
        else if(event.getSource()==btnSave){
            String nlieu=setLieus.getText();
            TableViewSelectionModel selectionModel = tableUpdt.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
            PreparedStatement pre;
        pre = con.prepareStatement("UPDATE larvard.`event` SET local= ? WHERE nom= ?;");
        pre.setString(1, nlieu);
         pre.setString(2, tableUpdt.getItems().get(row).getNom());
         pre.executeUpdate();
         refresh();
                 
                    
        }
        
        }

    @FXML
    private void trieClick(ActionEvent event) {
        try {
            data = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event ORDER BY date";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"),rs.getInt("nbpart")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDEvt.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("idevent"));
                   Lieu.setCellValueFactory(new PropertyValueFactory<Evenement, String>("local"));
                  Date.setCellValueFactory(new PropertyValueFactory<Evenement, String>("date"));
                    Nom.setCellValueFactory(new PropertyValueFactory<Evenement, String>("nom"));
                    Participants.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("nbpart"));
                    tabEvenement.setItems(data);
        
    }

    @FXML
    private void rechercheClick(ActionEvent event) {
        String nom =areaRecherche.getText();
        
        try {
            data = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event where nom= '"+ nom +"'";
            //String sql = "select * from evenement WHERE nom LIKE '%" + recherche + "%'";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"),rs.getInt("nbpart")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDEvt.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("idevent"));
                   Lieu.setCellValueFactory(new PropertyValueFactory<Evenement, String>("local"));
                  Date.setCellValueFactory(new PropertyValueFactory<Evenement, String>("date"));
                    Nom.setCellValueFactory(new PropertyValueFactory<Evenement, String>("nom"));
                    Participants.setCellValueFactory(new PropertyValueFactory<Evenement, Integer>("nbpart"));
                    tabEvenement.setItems(data);
        
    }

    @FXML
    private void Acceuil2(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));             

    }
    }
    
//public List<Product> RechercheProduitParNom(String recherche) {
//
//        List ALLproducts = new ArrayList();
//        try {
//            String query = "select * from product WHERE nom LIKE '%" + recherche + "%' AND stock > 0 OR stock=null;";
//            ste = con.createStatement();
//            ResultSet rest = ste.executeQuery(query);
//            while (rest.next()) {
//                Product pr = new Product();
//
//                pr.setId(rest.getInt("id"));
//                pr.setNom(rest.getString("Nom"));
//                pr.setPrice(rest.getInt("price"));
//                pr.setStock(rest.getInt("stock"));
//                pr.setQuantite(rest.getInt("quantite"));
//                pr.setImg(rest.getString("img"));
//                pr.setCategorie(rest.getString("Categorie"));
//                pr.setDescription(rest.getString("description"));
//                pr.setAge(rest.getInt("age"));
//                pr.setGenre(rest.getString("genre"));
//                ALLproducts.add(pr);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(ServiceProduct.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return ALLproducts;
//
//    }
