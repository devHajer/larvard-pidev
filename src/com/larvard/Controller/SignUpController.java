/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import com.larvard.Service.Password;
import com.larvard.Entity.User;
import com.larvard.Service.UserService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class SignUpController implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private TextField username;
    private TextField num;
    @FXML
    private Button signup;
    @FXML
    private Label labelemail;
    @FXML
    private Label labelusername;
    @FXML
    private Label labeldate;
    @FXML
    private ImageView emailTick;
    @FXML
    private ImageView usernameTick;
    @FXML
    private ImageView mdpTick;
    @FXML
    private ImageView mdp2Tick;
    @FXML
    private PasswordField mdp;
    @FXML
    private PasswordField cmdp;
    @FXML
    private Label labelcmdp;
    
    Boolean verificationUserNom = false;
    Boolean verificationUserPrenom = false;
    Boolean verificationUserEmail = false;
    Boolean verificationUserUsername = false;
    Boolean verificationUserMdp = false;
    Boolean verificationUserMdp2 = false;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
          nom.setVisible(true);
        prenom.setVisible(true);
        email.setVisible(true);
        username.setVisible(true);
        mdp.setVisible(true);
        cmdp.setVisible(true);
        emailTick.setVisible(false);
        usernameTick.setVisible(false);
        mdpTick.setVisible(false);
        mdp2Tick.setVisible(false);
    }    

    @FXML
    private void controlNom(KeyEvent event) {
            if (nom.getText().trim().equals("")) {

            verificationUserNom = false;

        } else {

            verificationUserNom = true;
        }
    }

    @FXML
    private void controlPrenom(KeyEvent event) {
             if (prenom.getText().trim().equals("")) {

            verificationUserPrenom = false;

        } else {

            verificationUserPrenom = true;
        }
    }

    @FXML
    private void controlEmail(KeyEvent event) {
               UserService ps = new UserService();
        if (ps.findUserByEmail(email.getText().trim()) == true) {
            labelemail.setText("Email Existe déja");
            verificationUserEmail = false;
        }
        if (ps.findUserByEmail(email.getText().trim()) == false) {//alphanumerique@alphanumerique.com
            //{ici longeur  }
            //debut ^
            //fin $
            String email_pattern = "^[a-zA-Z]+[a-zA-Z0-9\\._-]*[a-zA-Z0-9]@[a-zA-Z]+" + "[a-zA-Z0-9\\._-]*[a-zA-Z0-9]+\\.[a-zA-Z]{2,4}$";
            Pattern pattern = Pattern.compile(email_pattern);
            Matcher matcher = pattern.matcher(email.getText());

            if (matcher.matches()) {       //if   matcher ne contient pas la format   
                labelemail.setVisible(true);
                labelemail.setText("Email valide !");
                verificationUserEmail = true;
                emailTick.setVisible(true);
                labelemail.setText("");

            } else {
                labelemail.setVisible(true);
                labelemail.setText("Email Format invalide !");
                // JOptionPane.showMessageDialog(null, "Email Format invalide");
                verificationUserEmail = false;

            }
        }
    }

    @FXML
    private void controlUsername(KeyEvent event) {
          UserService ps = new UserService();
        if (ps.findUserByEmail(username.getText().trim()) == true) {
            labelusername.setText("Username Existe déja");
            verificationUserUsername = false;
        } else {
            verificationUserUsername = true;
            labelusername.setText("");
            usernameTick.setVisible(true);
        }

    }

   


    @FXML
    private void signup(ActionEvent event) throws IOException {
     if (verificationUserNom == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            alert.setContentText("Veuillez remplir le nom");
            alert.show();

        } else if (verificationUserPrenom == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            alert.setContentText("Veillez remplir le prenom");
            alert.show();

        } else if (verificationUserEmail == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            alert.setContentText("Veuillez remplir l'email");
            alert.show();

        } else if (verificationUserUsername == false) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            alert.setContentText("Veuillez remplir le nom de l'utilisateur");
            alert.show();

        } else if ((verificationUserMdp == false) && (verificationUserMdp2 == false)) {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            alert.setContentText("Veuillez resaissir votre Mot de Passe Correctement");
            alert.show();

        }  else {

            Password md = new Password();
            String mdpCrypte1 = md.hashPassword(mdp.getText());
            User user = new User();
            user.setUsername(username.getText());
            user.setEmail(email.getText());
            user.setPassword(mdpCrypte1);
            user.setNom(nom.getText());
            user.setPrenom(prenom.getText());
            UserService us=new UserService();
            us.ajouterUser(user);
           /*TrayNotification tray = new TrayNotification("Successfully",
                    "Inscription Effectuée avec Succés", NotificationType.SUCCESS);
            tray.setAnimationType(AnimationType.SLIDE);
            tray.showAndDismiss(Duration.seconds(10));*/

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/SignIn.fxml"));
           Parent root = loader.load();
            SignInController signin = loader.getController();
           prenom.getScene().setRoot(root);

        }
    }






    @FXML
    private void controlMDP(KeyEvent event) {
             if (mdp.getText().trim().equals("")) {

            verificationUserMdp = false;

        } else {

            verificationUserMdp = true;

        }
    }

    @FXML
    private void controlMDP2(KeyEvent event) {
             if (cmdp.getText().trim().equals("")) {

            verificationUserMdp2 = false;

        } else if (!cmdp.getText().equals(mdp.getText())) {
            verificationUserMdp2 = false;

            labelcmdp.setText("Resaisissez votre Mot de Passe Correctement");

        } else {

            verificationUserMdp2 = true;
            labelcmdp.setText("");
            mdpTick.setVisible(true);
            mdp2Tick.setVisible(true);
        }
    }

    @FXML
    private void precedent(ActionEvent event) {
             try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/SignIn.fxml"));
            Parent root = loader.load();
            SignInController signin = loader.getController();
            prenom.getScene().setRoot(root);
        } catch (IOException ex) {

        }
    }
    
    
}
