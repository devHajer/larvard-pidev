/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.larvard.Entity.Commande;
import com.larvard.Service.ServiceCommande;
import com.larvard.test.LarvardPI;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

/**
 *
 * @author mouhamed
 */
public class ButtonDeleteCommande extends TableCell<Disposer.Record, Boolean> {
      final Button cellButton = new Button("supprimer");
        
         ServiceCommande service_pr=new ServiceCommande();
        ButtonDeleteCommande(){
        
        	//Action when the button is pressed
            cellButton.setOnAction((ActionEvent t) -> {
                try {
                    // get Selected Item
                    Commande Produitcourant = (Commande) ButtonDeleteCommande.this.getTableView().getItems().get(ButtonDeleteCommande.this.getIndex());
                    //remove selected item from the table list
                    ObservableList<Commande> list= FXCollections.observableArrayList();
                    try {
                        for (Commande p:service_pr.readAllCommande())
                        {
                            list.add(p);
                            
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(ButtonDeleteCommande.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println(Produitcourant);
                    list.remove(Produitcourant);
                    try {
                        service_pr.deleteCommande(Produitcourant.getId());
                    } catch (SQLException ex) {
                        Logger.getLogger(ButtonDeleteCommande.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println(list);
                    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherCommandeFXML.fxml"))));
                } catch (IOException ex) {
                    Logger.getLogger(ButtonDeleteCommande.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
               // setGraphic(cellButton2);
                setGraphic(cellButton);
            }
        }
}
