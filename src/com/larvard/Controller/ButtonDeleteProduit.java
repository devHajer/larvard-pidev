/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.sun.prism.impl.Disposer.Record;
import com.larvard.Entity.Product;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

import com.larvard.Service.ServiceProduct;
import com.larvard.test.LarvardPI;
import java.sql.SQLException;

/**
 *
 * @author mouhamed
 */

class ButtonDeleteProduit extends TableCell<Record, Boolean> {
        final Button cellButton = new Button("supprimer");
        
         ServiceProduct service_pr=new ServiceProduct();
        ButtonDeleteProduit(){
        
        	//Action when the button is pressed
            cellButton.setOnAction(new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent t) {
                    try {
                        // get Selected Item
                        Product Produitcourant = (Product) ButtonDeleteProduit.this.getTableView().getItems().get(ButtonDeleteProduit.this.getIndex());
                        //remove selected item from the table list
                        ObservableList<Product> list= FXCollections.observableArrayList();
                        for (Product p:service_pr.AfficherProduit() )
                        {
                            list.add(p);
                            
                        }
                        System.out.println(Produitcourant);
                        list.remove(Produitcourant);
                        try {
                            service_pr.deleteProduit(Produitcourant);
                        } catch (SQLException ex) {
                            Logger.getLogger(ButtonDeleteProduit.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        System.out.println(list);
                        LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));             
                    } catch (IOException ex) {
                        Logger.getLogger(ButtonDeleteProduit.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            });
        }
        

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
               // setGraphic(cellButton2);
                setGraphic(cellButton);
            }
        }
    }