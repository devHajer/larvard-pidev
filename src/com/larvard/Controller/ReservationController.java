/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Reservation;
import com.larvard.Service.ServiceReservation;
import com.larvard.utils.DataBase;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.larvard.Entity.User;
import java.io.*;
import javafx.scene.layout.AnchorPane;
/**
 * FXML Controller class
 *
 * @author Bechir
 */
public class ReservationController implements Initializable {
private User user;
      @FXML
    private AnchorPane anchor;
    
    private Connection con;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private Button btnAjout;
    @FXML
    private Button btnAfficher;
    @FXML
    private Button Modifier;
    @FXML
    private GridPane onModifier;
    @FXML
    private JFXTextField setLieus;
    @FXML
    private Button btnModifier;
    @FXML
    private Button btnSave;
    @FXML
    private TableView<Reservation> tableUpdtR;
    @FXML
    private TableColumn<Reservation, Integer> IDReservs;
    @FXML
    private TableColumn<Reservation, Integer> IDEvts;
    @FXML
    private TableColumn<Reservation, String> Noms;
    @FXML
    private TableColumn<Reservation, String> Prenoms;
    @FXML
    private TableColumn<Reservation, Integer> Prixs;
    @FXML
    private TableColumn<Reservation, Integer> Quantites;
    @FXML
    private GridPane onReservation;
    @FXML
    private TableView<Reservation> tabReservation;
    @FXML
    private TableColumn<Reservation, Integer> IDReserv;
    @FXML
    private TableColumn<Reservation, Integer> IDEvt;
    @FXML
    private TableColumn<Reservation, String> Nom;
    @FXML
    private TableColumn<Reservation, String> Prenom;
    @FXML
    private TableColumn<Reservation, Integer> Prix;
    @FXML
    private TableColumn<Reservation, Integer> Quantite;
    @FXML
    private Button btnAjouter;
    @FXML
    private GridPane onAjout;
    @FXML
    private Label areaControle;
    @FXML
    private ComboBox<Integer> comboPrix;
    @FXML
    private TextField areaQuantite;
    @FXML
    private TextField areaNom;
    @FXML
    private TextField areaPrenom;
    @FXML
    private Button btnajouterr;

   public ObservableList<Reservation> data;
   public ObservableList<String> datan;
   ServiceReservation serreserv = new ServiceReservation() ;
   
   ObservableList<Integer> dat = FXCollections.observableArrayList(5,10);

        /**
     * @return the anchor
     */
    public AnchorPane getAnchor() {
        return anchor;
    }

    /**
     * @param anchor the anchor to set
     */
    public void setAnchor(AnchorPane anchor) {
        this.anchor = anchor;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Initializes the controller class.
     */

   ObservableList<Reservation> list = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> comboNom;
    @FXML
    private Label labelTotal;
    @FXML
    private TextField areaIDUser;
    @FXML
    private TableColumn<Reservation, Integer> IDUser;
    @FXML
    private TableColumn<Reservation, Integer> IDUsers;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        comboPrix.setItems(dat);
        
        comboNom.setItems(retour());
        

        
    }    
   
     public ReservationController() {
      con = DataBase.getInstance().getConnection();
     
     }
      public ObservableList<String> retour(){
        try {
            datan = FXCollections.observableArrayList();
            String sql = "SELECT * FROM event";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                datan.add(rs.getString("nom"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
    }
        return datan;
      }
    @FXML
    private void handleClick(ActionEvent event) {
        
        if(event.getSource() ==btnAjout)
        {
           lblStatusMini.setText("/Home/Ajouter Reservation");
           lblStatus.setText("Reservation");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 63, 43,99),CornerRadii.EMPTY,Insets.EMPTY)));
          onAjout.toFront();
          
        }
        else if(event.getSource() ==btnAfficher)
        {
           lblStatusMini.setText("/Home/Afficher");
           lblStatus.setText("Afficher Reservation");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 43,99,63),CornerRadii.EMPTY,Insets.EMPTY)));
          onReservation.toFront();
           try {
            data = FXCollections.observableArrayList();
            int id=user.getId();
               System.out.println("heatha howa"+user.toString());
            String sql = "SELECT * FROM participation where iduser = '"+ id +"'" ;
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Reservation(rs.getInt("idparticipation"),rs.getInt("idevent"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("prix"),rs.getInt("quantite"),rs.getInt("iduser")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDReserv.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("idparticipation"));
                   IDEvt.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("idevent"));
                  Nom.setCellValueFactory(new PropertyValueFactory<Reservation, String>("nom"));
                    Prenom.setCellValueFactory(new PropertyValueFactory<Reservation, String>("prenom"));
                    Prix.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("prix"));
                    Quantite.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("quantite"));
                    IDUser.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("iduser"));
                    tabReservation.setItems(data);
                   
        }
        
        
        else if(event.getSource() ==Modifier)
        {
           lblStatusMini.setText("/Home/Modifier");
           lblStatus.setText("Modifier Nom & prenom");
          pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb( 99, 43,38),CornerRadii.EMPTY,Insets.EMPTY)));
          onModifier.toFront();
          try {
            data = FXCollections.observableArrayList();
            //int id=user.getId();
                        int id=1;
            String sql = "SELECT * FROM participation where IdParticipation ='"+ id +"'" ;
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Reservation(rs.getInt("IdParticipation"),rs.getInt("idevent"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("prix"),rs.getInt("quantite"),rs.getInt("iduser")));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
                 IDReservs.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("IdParticipation"));
                   IDEvts.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("idevent"));
                  Noms.setCellValueFactory(new PropertyValueFactory<Reservation, String>("nom"));
                    Prenoms.setCellValueFactory(new PropertyValueFactory<Reservation, String>("prenom"));
                    Prixs.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("prix"));
                    Quantites.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("quantite"));
                    IDUsers.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("iduser"));
                    tableUpdtR.setItems(data);
      }
    
        
        
    }

    @FXML
    private void updateClick(ActionEvent event) throws SQLException {
        if(event.getSource()==btnModifier){
//            Evenement e = (Evenement) tableUpdt.getSelectionModel().getSelectedItems();
            //setLieu.setText(e.getLieu());
TableView.TableViewSelectionModel selectionModel = tableUpdtR.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
                  String x=tableUpdtR.getItems().get(row).getNom();
                  //String y=tableUpdt.getItems().get(row).getNom();
                  setLieus.setText(x);
            
        }
        else if(event.getSource()==btnSave){
            String nlieu=setLieus.getText();
            TableView.TableViewSelectionModel selectionModel = tableUpdtR.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
            PreparedStatement pre;
        pre = con.prepareStatement("UPDATE larvard.`participation` SET nom= ? WHERE IdParticipation= ?;");
        pre.setString(1, nlieu);
         pre.setInt(2, tableUpdtR.getItems().get(row).getIdreserv());
         pre.executeUpdate();
         refresh();
        }
    }
public void refresh(){
     data.clear();
     try {
            data = FXCollections.observableArrayList();
            int id=user.getId();
            String sql = "SELECT * FROM participation where IdParticipation ='"+ id +"'" ;
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Reservation(rs.getInt("IdParticipation"),rs.getInt("idevent"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("prix"),rs.getInt("quantite"),rs.getInt("iduser")));
            }
            tableUpdtR.setItems(data);
        } catch (SQLException e) {
            e.printStackTrace();

        }
 }
    @FXML
    private void supprimer(ActionEvent event) throws SQLException {
        
        TableView.TableViewSelectionModel selectionModel = tabReservation.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();
                TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                int row = tablePosition.getRow();
                  int x=tabReservation.getItems().get(row).toStringid();
                serreserv.delete1(x);
//                for ( int i = 0; i<tabEvenement.getItems().size(); i++) {
//    tabEvenement.getItems().clear();
//}
                ObservableList<Reservation> all,single ;
        all=tabReservation.getItems() ;
        single=tabReservation.getSelectionModel().getSelectedItems() ;
        // act= new Activite() ;
        //remove=seract.delete(single) ;
        single.forEach(all::remove);
        
    }

    @FXML
    private void ajouter(ActionEvent event) throws SQLException, FileNotFoundException, DocumentException {
       Reservation r = new Reservation();  
       Document doc = new Document();
       PdfWriter.getInstance(doc, new FileOutputStream("Reservation.pdf"));
                            System.out.println("hetha user houni"+user.toString());

        
           r.setNom(user.getUsername());
           r.setPrenom(user.getPrenom());
           String events=comboNom.getValue();
           r.setQuantite(Integer.parseInt(areaQuantite.getText()));
           r.setIduser(user.getId());
           r.setPrix(comboPrix.getValue());
           
           //Integer.parseInt(tfDuree.getText())
           serreserv.ajouter3(r, events);
           doc.open();
           doc.add(new Paragraph("Reservation de mr" + areaPrenom.getText() + "!!"));
          doc.close();
           
                   
    }

    @FXML
    private void totalClick(ActionEvent event) {
        int total = 0;
        try {
            data = FXCollections.observableArrayList();
            int id=user.getId();
            String sql = "SELECT * FROM participation ";
            PreparedStatement stat = con.prepareStatement(sql);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                data.add(new Reservation(rs.getInt("IdParticipation"),rs.getInt("idevent"),rs.getString("nom"),rs.getString("prenom"),rs.getInt("prix"),rs.getInt("quantite"),rs.getInt("iduser")));
                total+=(rs.getInt("quantite")*rs.getInt("prix"));
               
            }
            labelTotal.setText("Total:\t" + Integer.toString(total) + "\tdt");
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


}
