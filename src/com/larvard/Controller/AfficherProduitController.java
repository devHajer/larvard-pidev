/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import com.larvard.Entity.Product;
import com.larvard.Service.ServiceProduct;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import com.sun.prism.impl.Disposer;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class AfficherProduitController implements Initializable {

    @FXML
    private JFXButton Rating;
    @FXML
    private JFXButton Commande;
    @FXML
    private JFXButton Statistique;
    @FXML
    private JFXButton Ajouter;
         public AfficherProduitController() {
        con = DataBase.getInstance().getConnection();

    }
        ServiceProduct service_pr=new ServiceProduct();
ObservableList<Product> list= FXCollections.observableArrayList();

Connection con;
    
    
    
    
    
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private TableColumn<Product, String> col_nom;
    @FXML
    private TableView<Product> tableview;
    @FXML
    private TableColumn<Product, String> col_id;
    @FXML
    private TableColumn<Product, String> col_price;
    @FXML
    private TableColumn<Product, String> col_categorie;
    @FXML
    private TableColumn<Product, String> col_genre;
    @FXML
    private TableColumn<Product, String> col_descreption;
    @FXML
    private TableColumn<Product, String> col_image;
    @FXML
    private TableColumn<Product, String> col_age;
    @FXML
    private TableColumn<Product, String> col_rating;
    @FXML
    private TableColumn<Product, String> col_stock;
   @FXML
    private TableColumn<Product, String> col_quantite;
    /**
     * Initializes the controller class.
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
     for (Product p:service_pr.AfficherProduit() )
        {
            list.add(p);
            
        }

      
        col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        col_price.setCellValueFactory(new PropertyValueFactory<>("price"));
        col_categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));
        col_genre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        col_descreption.setCellValueFactory(new PropertyValueFactory<>("description"));
        col_image.setCellValueFactory(new PropertyValueFactory<>("img"));
        col_age.setCellValueFactory(new PropertyValueFactory<>("age"));
        col_rating.setCellValueFactory(new PropertyValueFactory<>("rating"));
        col_stock.setCellValueFactory(new PropertyValueFactory<>("stock"));
        col_quantite.setCellValueFactory(new PropertyValueFactory<>("quantite"));
        
        TableColumn col_action = new TableColumn<>("supprimer");
        tableview.getColumns().add(col_action);
        
        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, 
                ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

    
                    
             @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new ButtonDeleteProduit();
            }
        
        });
                //modifier
        TableColumn col_modifier = new TableColumn<>("modifier");
        tableview.getColumns().add(col_modifier);
        
        col_modifier.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>, 
                ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        //Adding the Button to the cell
        col_modifier.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

            @Override
            public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                return new ButtonUpdateProduit();
            }
        
        });
                tableview.setItems(list);

    }

    @FXML
    private void ButtonAccuiel(ActionEvent event) throws IOException {
   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));              
    }

    @FXML
    private void BottonRating(ActionEvent event) throws IOException {
            LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/.fxml"))));             

    }

    @FXML
    private void ButtonCommande(ActionEvent event) throws IOException {
    LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherCommandeFXML.fxml"))));             

    }

    @FXML
    private void ButtonStaistique(ActionEvent event) throws IOException {
            LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/stat.fxml"))));             
    }

    @FXML
    private void ButtonAjouterProduit(ActionEvent event) throws IOException {
            LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));             
    }

}
