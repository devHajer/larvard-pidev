/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXTextField;
import com.larvard.Entity.Epreuve;
import com.larvard.Service.EpreuveService;
import com.larvard.test.LarvardPI;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author Abn
 */
public class EpreuveFXMLController implements Initializable {

    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    @FXML
    private TableView<Epreuve> tableview;
    @FXML
    private TableColumn<Epreuve, String> reference;
    @FXML
    private TableColumn<Epreuve, String> dateDeb;
    @FXML
    private TableColumn<Epreuve, Integer> duree;
    @FXML
    private TableColumn<Epreuve, Integer> niveau;
    @FXML
    private TableColumn<Epreuve, String> type;

    private EpreuveService es ;
    @FXML
    private JFXTextField tf_recherche;
    @FXML
    private HBox ActionBox;
    
    
    public static Epreuve epreuve ;
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            
        ActionBox.setDisable(true);
         es = new EpreuveService();
        
        reference.setCellValueFactory(new PropertyValueFactory<>("reference"));
        dateDeb.setCellValueFactory(new PropertyValueFactory<>("dateEp"));
        niveau.setCellValueFactory(new PropertyValueFactory<>("niveau"));
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        duree.setCellValueFactory(new PropertyValueFactory<>("duree"));
        
        tableview.getItems().setAll(es.readAll());
           
        
        tableview.getSelectionModel().selectedItemProperty().addListener( new ChangeListener<Epreuve>() {
            @Override
            public void changed(ObservableValue observable, Epreuve oldValue, Epreuve newValue) {
                   if(tableview.getSelectionModel().getSelectedItem() != null )
                     ActionBox.setDisable(false);
                   else ActionBox.setDisable(true);
            }
   
     });
        
        
        
        } catch (SQLException ex) {
            Logger.getLogger(EpreuveFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }    

    @FXML
    private void rechercheAction() {
        tableview.getSelectionModel().clearSelection();
        System.out.println(tf_recherche.getText());
        tableview.getItems().setAll(es.getEpreuveByAll(tf_recherche.getText()));
        
    }

    @FXML
    private void modifier() {
       epreuve = new Epreuve();
       epreuve = tableview.getSelectionModel().getSelectedItem();
        try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/ModifierEpreuve.fxml"));
        Stage stage = (Stage) ActionBox.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void supprimer() {
        try {
            es.delete(tableview.getSelectionModel().getSelectedItem().getIdEp());
             tableview.getItems().setAll(es.readAll());
//                     AlertMaker.sendNotificationApi("Epreuve supprimée avec succées", "");

        } catch (SQLException ex) {
            Logger.getLogger(EpreuveFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void Ajouter() {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/AjouterEpreuve.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEpreuveBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EpreuveList.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }

    @FXML
    private void GestionEvaluationBtn(ActionEvent event) {
          try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/larvard/Gui/EvaluationList.fxml"));
        Stage stage = (Stage) tf_recherche.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }catch (IOException io){
        io.printStackTrace();
    }
    }
    
        @FXML
    private void AccueilBtn(ActionEvent event) throws IOException {
                     LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));             

    }
    
}
