/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import com.larvard.Service.ServiceStat;
import com.larvard.test.LarvardPI;
import com.larvard.utils.DataBase;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author mouhamed
 */
public class StatProduits implements Initializable {

    ObservableList<PieChart.Data> stat=FXCollections.observableArrayList();

    ArrayList<String> libelle = new ArrayList<String>();
    ArrayList<Integer> quantiteDispo = new ArrayList<Integer>();
    @FXML
    private PieChart piechart;
    @FXML
    private BarChart<String, Integer> barchart;
    ServiceStat service_stat= new ServiceStat();
    @FXML
    private JFXButton Accueil;
    @FXML
    private JFXButton Produits;
    @FXML
    private JFXButton Rating;
    @FXML
    private JFXButton Commande;
    @FXML
    private JFXButton Statistique;
    @FXML
    private JFXButton Ajouter;
    @FXML
    private JFXButton Ajouter1;
    @FXML
    private Pane pnlStatus;
    @FXML
    private Label lblStatusMini;
    @FXML
    private Label lblStatus;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
try {
           
            
            Statement stm =  DataBase.getInstance().getConnection().createStatement();
               ResultSet rest=stm.executeQuery("select nom , quantite from product");
              
               while(rest.next())
               {
                   libelle.add(rest.getString("nom"));
                   quantiteDispo.add(rest.getInt("quantite"));
                   stat.add(new PieChart.Data(rest.getString("nom"), rest.getInt("quantite")));
               }
    }
    catch (SQLException ex) {
            Logger.getLogger(StatProduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        piechart.setAnimated(true);
        piechart.maxHeight(1000);
       piechart.setData(stat);
        XYChart.Series<String, Integer> series1 = new XYChart.Series<>();
  
        for (Entry<String,Integer> i: service_stat.getStat().entrySet() ) {
            String nom=i.getKey();
            int nbr=i.getValue();
            XYChart.Data<String, Integer> d = new XYChart.Data<>(nom, nbr);
            series1.getData().add(d);
        }
        
        barchart.getData().addAll(series1);
       
    }

    @FXML
    private void ButtonAccuiel(ActionEvent event) throws IOException {
           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/PannellAdmin.fxml"))));              

    }

    @FXML
    private void ButtonProduit(ActionEvent event) throws IOException {
           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void BottonRating(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml"))));              

    }

    @FXML
    private void ButtonCommande(ActionEvent event) throws IOException {
                   LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherCommandeFXML.fxml"))));              

    }

    @FXML
    private void ButtonStaistique(ActionEvent event) throws IOException {
        
                           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/stat.fxml"))));              

    }

    @FXML
    private void ButtonAjouterProduit(ActionEvent event) throws IOException {
                           LarvardPI.getInstance().changescene(new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml"))));              

    }
    
    
}