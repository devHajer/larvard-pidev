/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Entity;

/**
 *
 * @author mouhamed
 */
public class Rating {
    
    private int id;
    private int rating;
     private String nom_produit;
    private int idproduit;
    private int idUser;

    public Rating(int id, int rating, String nom_produit, int idproduit, int idUser) {
        this.id = id;
        this.rating = rating;
        this.nom_produit = nom_produit;
        this.idproduit = idproduit;
        this.idUser = idUser;
    }

    public Rating(int rating, String nom_produit, int idproduit, int idUser) {
        this.rating = rating;
        this.nom_produit = nom_produit;
        this.idproduit = idproduit;
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public int getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(int idproduit) {
        this.idproduit = idproduit;
    }

    @Override
    public String toString() {
        return "Rating{" + "id=" + id + ", rating=" + rating + ", nom_produit=" + nom_produit + ", idproduit=" + idproduit + ", idUser=" + idUser + '}';
    }
    
    
}
