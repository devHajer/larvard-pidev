/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author mouhamed
 */
//id	price	genre	description	img	age	rating	stock	Categorie

public class Product {
    
    private static int id_courant ;

    private int id;
    private String nom;
    private double price;
    private String categorie;
    private String genre;
    private String description;
    private String img;
    private int age;
    private int rating;
    private int stock;
    private int quantite;
    
public Product()
{
}

    public Product(int id, String nom, double price, String categorie, String genre, String description, String img, int age, int stock, int quantite) {
        this.id = id;
        this.nom = nom;
        this.price = price;
        this.categorie = categorie;
        this.genre = genre;
        this.description = description;
        this.img = img;
        this.age = age;
        this.stock = stock;
        this.quantite = quantite;
    }

    public Product(int id, String nom, double price, String categorie, String genre, String description, String img, int age, int rating, int stock, int quantite) {
        this.id = id;
        this.nom = nom;
        this.price = price;
        this.categorie = categorie;
        this.genre = genre;
        this.description = description;
        this.img = img;
        this.age = age;
        this.rating = rating;
        this.stock = stock;
        this.quantite = quantite;
    }

    public Product(String nom, double price, String categorie, String genre, String description, String img, int age, int rating, int stock, int quantite) {
        this.nom = nom;
        this.price = price;
        this.categorie = categorie;
        this.genre = genre;
        this.description = description;
        this.img = img;
        this.age = age;
        this.rating = rating;
        this.stock = stock;
        this.quantite = quantite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public static ObservableList<Product> getPanier() {
        return Panier;
    }

    public static void setPanier(ObservableList<Product> Panier) {
        Product.Panier = Panier;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", nom=" + nom + ", price=" + price + ", categorie=" + categorie + ", genre=" + genre + ", description=" + description + ", img=" + img + ", age=" + age + ", rating=" + rating + ", stock=" + stock + ", quantite=" + quantite + '}';
    }




public static ObservableList<Product> Panier = FXCollections.observableArrayList();  
  public static class getPanier {

        public getPanier() {
        }
    }

     public static boolean setPanier(Product P) {
        Boolean Test=true;
        
    	for (int i = 0; i < Panier.size(); i++) {
        if(Panier.get(i).getId()==P.getId())
        {
                        Test=false;
                        Panier.get(i).setQuantite(Panier.get(i).getQuantite()+1);
                        P.setQuantite(P.getQuantite()+1);
                      //  System.out.println( Panier.get(i));
                     
        }
		}
       if(Test==true)
            {
             P.setQuantite(P.getQuantite()+1);
           Panier.add(P);
           
            }
     return Test;
    }
    public static int getId_courant() {
        return id_courant;
    }

    public static void setId_courant(int id_courant) {
        Product.id_courant = id_courant;
    }
    


    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
  
}
