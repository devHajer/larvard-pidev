/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author ivaxs
 */
public class Evenement {
      private int idevt;
    private String nom;
    private String lieu;
    private String date;
    private int nbpart;
    private java.sql.Date dates;
    private Timestamp datet;

    public Evenement(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDatet(Timestamp datet) {
        this.datet = datet;
    }

    public Timestamp getDatet() {
        return datet;
    }

    public Evenement(int idevt, String nom, String lieu, int nbpart, Timestamp datet) {
        this.idevt = idevt;
        this.nom = nom;
        this.lieu = lieu;
        this.nbpart = nbpart;
        this.datet = datet;
    }

    public Evenement(String nom, String lieu, Timestamp datet) {
        this.nom = nom;
        this.lieu = lieu;
        this.datet = datet;
    }

    public Evenement(String nom, String lieu, int nbpart, Timestamp datet) {
        this.nom = nom;
        this.lieu = lieu;
        this.nbpart = nbpart;
        this.datet = datet;
    }
    

    public Evenement(String nom, String lieu) {
        this.nom = nom;
        this.lieu = lieu;
    }

    public void setDates(Date dates) {
        this.dates = dates;
    }

    public Date getDates() {
        return dates;
    }

    public Evenement(String nom, String lieu, int nbpart, Date dates) {
        this.nom = nom;
        this.lieu = lieu;
        this.nbpart = nbpart;
        this.dates = dates;
    }
    

    public Evenement(int idevt, String nom, String lieu, String date, int nbpart) {
        this.idevt = idevt;
        this.nom = nom;
        this.lieu = lieu;
        this.date = date;
        this.nbpart = nbpart;
        
    }

    public Evenement(int idevt, String nom, String lieu, String date) {
        this.idevt = idevt;
        this.nom=nom;
        this.lieu = lieu;
        this.date = date;
        
    }

    public Evenement(String lieu, String nom, String date) {
        this.lieu = lieu;
        this.nom=nom;
        this.date = date;
    }

    public Evenement() {
        this.nbpart=0;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public int getIdevt() {
        return idevt;
    }

    public String getLieu() {
        return lieu;
    }

    public String getDate() {
        return date;
    }

    public int getNbpart() {
        return nbpart;
    }

   

    public void setIdevt(int idevt) {
        this.idevt = idevt;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setNbpart(int nbpart) {
        this.nbpart = nbpart;
    }

   

    @Override
    public String toString() {
        return "Evenement{" + "idevt=" + idevt + ", lieu=" + lieu + ", date=" + date + ", nbpart=" + nbpart + '}';
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Evenement other = (Evenement) obj;
        if (this.idevt != other.idevt) {
            return false;
        }
        return true;
    }
    public int toStringid()
    {
    return idevt;
    }

}
