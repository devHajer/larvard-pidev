/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */

   

   /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Date;
/**
 *
 * @author ivaxs
 */
public class Enfant {
      private int IDEnfant ; 
    private String nom ; 
    private String prenom ; 
    private Date DateNaissance ;
    private String Image ; 
    private Classe classe ; 
    private AfterSchool AfterSchool ; 

    public Enfant(String nom, String prenom, Date DateNaissance, String Image, Classe classe, AfterSchool AfterSchool) {
        this.nom = nom;
        this.prenom = prenom;
        this.DateNaissance = DateNaissance;
        this.Image = Image;
        this.classe = classe;
        this.AfterSchool = AfterSchool;
    }

    public Enfant(int IDEnfant, String nom, String prenom, Date DateNaissance, String Image, Classe classe, AfterSchool AfterSchool) {
        this.IDEnfant = IDEnfant;
        this.nom = nom;
        this.prenom = prenom;
        this.DateNaissance = DateNaissance;
        this.Image = Image;
        this.classe = classe;
        this.AfterSchool = AfterSchool;
    }

    public Enfant(int IDEnfant, String nom, String prenom, Date DateNaissance, AfterSchool AfterSchool) {
        this.IDEnfant = IDEnfant;
        this.nom = nom;
        this.prenom = prenom;
        this.DateNaissance = DateNaissance;
        this.AfterSchool = AfterSchool;
    }
    
    public  Enfant() {
        
    }

    public Enfant(int id, String nom, String prenom, String DateNaissance, int af) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Enfant(String nom, String prenom, String DateNaissance, String image, Classe classe, int AfterSchool) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIDEnfant() {
        return IDEnfant;
    }

    public void setIDEnfant(int IDEnfant) {
        this.IDEnfant = IDEnfant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return DateNaissance;
    }

    public void setDateNaissance(Date DateNaissance) {
        this.DateNaissance = DateNaissance;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public AfterSchool getAfterSchool() {
        return AfterSchool;
    }

    public void setAfterSchool(AfterSchool AfterSchool) {
        this.AfterSchool = AfterSchool;
    }

    @Override
    public String toString() {
        return "Enfant{" + "IDEnfant=" + IDEnfant + ", nom=" + nom + ", prenom=" + prenom + ", DateNaissance=" + DateNaissance + ", Image=" + Image + ", classe=" + classe + ", AfterSchool=" + AfterSchool + '}';
    }

    }

