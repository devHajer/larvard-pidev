/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Emploi {
      private int id ; 
    private String nom ; 
    private String description ; 
    private String Type ; 
    private int idEnseignant ; 

    public Emploi(int id, String nom, String description, String Type, int idEnseignant) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.Type = Type;
        this.idEnseignant = idEnseignant;
    }

    public Emploi(String nom, String description, String Type, int idEnseignant) {
        this.nom = nom;
        this.description = description;
        this.Type = Type;
        this.idEnseignant = idEnseignant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public int getIdEnseignant() {
        return idEnseignant;
    }

    @Override
    public String toString() {
        return "Emploi{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", Type=" + Type + ", idEnseignant=" + idEnseignant + '}';
    }

}
