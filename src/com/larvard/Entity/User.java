/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

import java.util.Collection;
import java.util.Date;



/**
 *
 * @author ivaxs
 */
public class User {
    
    private int id;
    private String username;
    private String email;
    private boolean enabled;
    private String salt;
    private String password;
    private Date lastLogin;
    private String confirmationToken;
    private String roles;
    private String nom;
    private String prenom;
  

    private Collection<Product> produitCollection;
    private Collection<Commande> commandeCollection;

    public User() {
    }

    public User(int id, String username, String email, boolean enabled, String salt, String password, Date lastLogin, String confirmationToken, String roles, String nom, String prenom, Collection<Product> produitCollection, Collection<Commande> commandeCollection) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.enabled = enabled;
        this.salt = salt;
        this.password = password;
        this.lastLogin = lastLogin;
        this.confirmationToken = confirmationToken;
        this.roles = roles;
        this.nom = nom;
        this.prenom = prenom;
        this.produitCollection = produitCollection;
        this.commandeCollection = commandeCollection;
    }

    public User(String username, String email, boolean enabled, String salt, String password, Date lastLogin, String confirmationToken, String roles, String nom, String prenom, Collection<Product> produitCollection, Collection<Commande> commandeCollection) {
        this.username = username;
        this.email = email;
        this.enabled = enabled;
        this.salt = salt;
        this.password = password;
        this.lastLogin = lastLogin;
        this.confirmationToken = confirmationToken;
        this.roles = roles;
        this.nom = nom;
        this.prenom = prenom;
        this.produitCollection = produitCollection;
        this.commandeCollection = commandeCollection;
    }

    public User(int id, String username, String email, String password, String roles, String nom, String prenom) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.nom = nom;
        this.prenom = prenom;
    }

    public User(String username, String email, String password, String confirmationToken, String prenom) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmationToken = confirmationToken;
        this.prenom = prenom;
    }

    public User(String username, String email, boolean enabled, String password, String roles, String nom, String prenom) {
        this.username = username;
        this.email = email;
        this.enabled = enabled;
        this.password = password;
        this.roles = roles;
        this.nom = nom;
        this.prenom = prenom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Collection<Product> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Product> produitCollection) {
        this.produitCollection = produitCollection;
    }

    public Collection<Commande> getCommandeCollection() {
        return commandeCollection;
    }

    public void setCommandeCollection(Collection<Commande> commandeCollection) {
        this.commandeCollection = commandeCollection;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", email=" + email + ", enabled=" + enabled + ", salt=" + salt + ", password=" + password + ", lastLogin=" + lastLogin + ", confirmationToken=" + confirmationToken + ", roles=" + roles + ", nom=" + nom + ", prenom=" + prenom + ", produitCollection=" + produitCollection + ", commandeCollection=" + commandeCollection + '}';
    }


  


 
}
