/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author mouhamed
 */
public class commande_product {
    private Product product;
    private Commande commande;
    

    public commande_product() {
    }

    public commande_product(Product product, Commande commande) {
        this.product = product;
        this.commande = commande;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @Override
    public String toString() {
        return "commande_product{" + "product=" + product + ", commande=" + commande + '}';
    }


    
}
