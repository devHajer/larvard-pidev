/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Activite {
    
    private int IdActivite ;
    private String titre; 
    private String description ;
    private int duree;
    private String type ;

    public Activite(int IdActivite, String titre, String description, int duree, String type) {
        this.IdActivite = IdActivite;
        this.titre = titre;
        this.description = description;
        this.duree = duree;
        this.type = type;
    }

    public  Activite(String titre) {
        this.titre = titre;
    }

    public Activite(int IdActivite, String titre) {
        this.IdActivite = IdActivite;
        this.titre = titre;
    }
    

    public Activite(String titre, String description, int duree, String type) {
        this.titre = titre;
        this.description = description;
        this.duree = duree;
        this.type = type;
    }

    public Activite() {
    }

    public final int getIdActivite() {
        return IdActivite;
    }

    public final void setIdActivite(int IdActivite) {
        this.IdActivite = IdActivite;
    }

    public final String getTitre() {
        return titre;
    }

    public final void setTitre(String titre) {
        this.titre = titre;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(String description) {
        this.description = description;
    }

    public final int getDuree() {
        return duree;
    }

    public final void setDuree(int duree) {
        this.duree = duree;
    }

    public  final String getType() {
        return type;
    }

    public final void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Activite{" + "IdActivite=" + IdActivite + ", titre=" + titre + ", description=" + description + ", duree=" + duree + ", type=" + type + '}';
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

  
   
  
  

 


    
}
