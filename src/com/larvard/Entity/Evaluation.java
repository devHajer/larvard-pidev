package com.larvard.Entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author ivaxs
 */
public class Evaluation {
     private int id ; 
    private int iduser ;
    private String appreciation ; 
    private int decision ; 
    private int idepreuve ; 
    private int idenfant;

    public int getIdenfant() {
        return idenfant;
    }

    public void setIdenfant(int idenfant) {
        this.idenfant = idenfant;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getAppreciation() {
        return appreciation;
    }

    public void setAppreciation(String appreciation) {
        this.appreciation = appreciation;
    }

    public int getDecision() {
        return decision;
    }

    public void setDecision(int decision) {
        this.decision = decision;
    }

    public int getIdepreuve() {
        return idepreuve;
    }

    public void setIdepreuve(int idepreuve) {
        this.idepreuve = idepreuve;
    }
    
    public Evaluation() {
    }

    @Override
    public String toString() {
        return "Evaluation{" + "id=" + id + ", enseignant=" + iduser + ", appreciation=" + appreciation + ", decision=" + decision + ", epreuve=" + idepreuve + ", enfant=" + idenfant + '}';
    }

    public int getIdEnseignant() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
    
    
    
    
}
