/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Note { 
     private int idNote ; 
    private int mat1 ; 
    private int mat2 ; 
    private int mat3 ; 
    private int mat4 ;  

    public Note(int idNote, int mat1, int mat2, int mat3, int mat4) {
        this.idNote = idNote;
        this.mat1 = mat1;
        this.mat2 = mat2;
        this.mat3 = mat3;
        this.mat4 = mat4;
    }

    public Note(int mat1, int mat2, int mat3, int mat4) {
        this.mat1 = mat1;
        this.mat2 = mat2;
        this.mat3 = mat3;
        this.mat4 = mat4;
    }

    public int getIdNote() {
        return idNote;
    }

    public void setIdNote(int idNote) {
        this.idNote = idNote;
    }

    public int getMat1() {
        return mat1;
    }

    public void setMat1(int mat1) {
        this.mat1 = mat1;
    }

    public int getMat2() {
        return mat2;
    }

    public void setMat2(int mat2) {
        this.mat2 = mat2;
    }

    public int getMat3() {
        return mat3;
    }

    public void setMat3(int mat3) {
        this.mat3 = mat3;
    }

    public int getMat4() {
        return mat4;
    }

    public void setMat4(int mat4) {
        this.mat4 = mat4;
    }

    
    
    
    
}
