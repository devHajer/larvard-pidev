/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;
import com.larvard.Entity.Lecon ;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import com.larvard.utils.DataBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;  
import com.larvard.IService.IServiceEmploi;
import com.larvard.IService.IServiceLecon;
import java.text.SimpleDateFormat;
import java.text.ParseException ;
/**
/**
 *
 * @author ivaxs
 */
public class Lecon {
     private int id ; 
    private String nom ; 
    private String description ; 
    private String DateLecon ;
    private int duree ; 
    private int idEmploi ; 
    private int idEnseignant  ;  
    private int idSalle ; 

    public Lecon(int id, String nom, String description, String DateLecon, int duree, int idEmploi, int idEnseignant, int idSalle) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.DateLecon = DateLecon;
        this.duree = duree;
        this.idEmploi = idEmploi;
        this.idEnseignant = idEnseignant;
        this.idSalle = idSalle;
    }

    public Lecon(String nom, String description, String DateLecon, int duree, int idEmploi, int idEnseignant, int idSalle) {
        this.nom = nom;
        this.description = description;
        this.DateLecon = DateLecon;
        this.duree = duree;
        this.idEmploi = idEmploi;
        this.idEnseignant = idEnseignant;
        this.idSalle = idSalle;
    }

    public int getId() {
        return id;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateLecon() {
        return DateLecon;
    }

    public void setDateLecon(String DateLecon) {
        this.DateLecon = DateLecon;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public int getIdEmploi() {
        return idEmploi;
    }

    public int getIdEnseignant() {
        return idEnseignant;
    }

    public int getIdSalle() {
        return idSalle;
    }

    @Override
    public String toString() {
        return "Lecon{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", DateLecon=" + DateLecon + ", duree=" + duree + ", idEmploi=" + idEmploi + ", idEnseignant=" + idEnseignant + ", idSalle=" + idSalle + '}';
    }
    
    
}
