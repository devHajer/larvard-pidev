/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Reclamation {
       private int id;
    private int id_parent;
    private String date;
    private String type;
    private boolean etat;
    private String sujet;

    public Reclamation(int id, int id_parent, String date, String type, boolean etat, String sujet) {
        this.id = id;
        this.id_parent = id_parent;
        this.date = date;
        this.type = type;
        this.etat = etat;
        this.sujet = sujet;
    }

    public Reclamation(int id_parent, String date, String type, boolean etat, String sujet) {
        this.id_parent = id_parent;
        this.date = date;
        this.type = type;
        this.etat = etat;
        this.sujet = sujet;
    }




    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public int getId_parent() {
        return id_parent;
    }

    public String getDate() {
        return date;
    }

    public boolean isEtat() {
        return etat;
    }

    public String getSujet() {
        return sujet;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    @Override
    public String toString() {
        return "Reclamation{" + "id=" + id + ", id_parent=" + id_parent + ", date=" + date + ", etat=" + etat + ", sujet=" + sujet + '}';
    }
    
    
}
