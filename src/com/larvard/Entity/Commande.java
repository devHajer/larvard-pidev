/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author mouhamed
 */
public class Commande {
        private static int id_courant ;

private int id;
private Product idproduit;
private User user;
private Date datecom;
private double prixtotale;
private String valide;


    public static int getId_courant() {
        return id_courant;
    }

    public static void setId_courant(int id_courant) {
        Commande.id_courant = id_courant;
    }

    public Commande(int id, User user, Date datecom, double prixtotale, String valide) {
        this.id = id;
        this.user = user;
        this.datecom = datecom;
        this.prixtotale = prixtotale;
        this.valide = valide;
    }

    public Commande(User user, Date datecom, double prixtotale, String valide) {
        this.user = user;
        this.datecom = datecom;
        this.prixtotale = prixtotale;
        this.valide = valide;
    }
    

    public Commande() {
    }

    public Commande(int id, Product idproduit,User user, Date datecom, double prixtotale) {
        this.id = id;
        this.idproduit = idproduit;
        this.user = user;
        this.datecom = datecom;
        this.prixtotale = prixtotale;
    }

    public Commande(Product idproduit, User user, Date datecom, double prixtotale) {
        this.idproduit = idproduit;
        this.user = user;
        this.datecom = datecom;
        this.prixtotale = prixtotale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Product idproduit) {
        this.idproduit = idproduit;
    }



    public Date getDatecom() {
        return datecom;
    }

    public void setDatecom(Date datecom) {
        this.datecom = datecom;
    }

    public double getPrixtotale() {
        return prixtotale;
    }

    public void setPrixtotale(double prixtotale) {
        this.prixtotale = prixtotale;
    }


    public String getValide() {
        return valide;
    }

    public void setValide(String valide) {
        this.valide = valide;
    }

    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Commande{" + "id=" + id + ", idproduit=" + idproduit + ", user=" + user + ", datecom=" + datecom + ", prixtotale=" + prixtotale + ", valide=" + valide + '}';
    }



  
  
  
    

    
}







