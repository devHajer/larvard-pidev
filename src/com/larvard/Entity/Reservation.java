/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Entity;
import java.util.Objects;
/**
 *
 * @author Bechir
 */
public class Reservation {
    Evenement e;
    int idreserv;
    int idevtr;
    String nom;
    String prenom;
    int prix;
    int quantite;
    int iduser;

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public Reservation(int idreserv, int idevtr, String nom, String prenom, int prix, int quantite, int iduser) {
        this.idreserv = idreserv;
        this.idevtr = idevtr;
        this.nom = nom;
        this.prenom = prenom;
        this.prix = prix;
        this.quantite = quantite;
        this.iduser = iduser;
    }

   
    

    

    public Reservation(Evenement e, int idreserv, int idevtr, String nom, String prenom, int prix, int quantite) {
        this.e = e;
        this.idreserv = idreserv;
        this.idevtr = idevtr;
        this.nom = nom;
        this.prenom = prenom;
        this.prix = prix;
        this.quantite = quantite;
    }

    public Reservation(Evenement e, String nom, String prenom, int prix, int quantite) {
        this.e = e;
        this.nom = nom;
        this.prenom = prenom;
        this.prix = prix;
        this.quantite = quantite;
    }

    public Reservation(int idreserv, int idevtr, String nom, String prenom, int prix, int quantite) {
        this.idreserv = idreserv;
        this.idevtr = idevtr;
        this.nom = nom;
        this.prenom = prenom;
        this.prix = prix;
        this.quantite = quantite;
        
    }

    public Reservation() {
       
    }

    public Evenement getE() {
        return e;
    }

    public int getIdreserv() {
        return idreserv;
    }

    public int getIdevtr() {
        return idevtr;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public int getPrix() {
        return prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setE(Evenement e) {
        this.e = e;
    }

    public void setIdreserv(int idreserv) {
        this.idreserv = idreserv;
    }

    public void setIdevtr(int idevtr) {
        this.idevtr = idevtr;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.idreserv;
        hash = 71 * hash + Objects.hashCode(this.nom);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation other = (Reservation) obj;
        if (this.idreserv != other.idreserv) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reservation{" + "e=" + e + ", idreserv=" + idreserv + ", idevtr=" + idevtr + ", nom=" + nom + ", prenom=" + prenom + ", prix=" + prix + ", quantite=" + quantite + '}';
    }
    public int toStringid()
    {
    return idreserv;
    }
}
