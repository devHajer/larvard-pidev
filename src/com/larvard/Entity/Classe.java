/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Classe {
       private int id ; 
    private String nom ; 
    private String type ; 
    private int capacite ;
    private int idEmploi; 

    public Classe(int id, String nom, String type, int capacite, int idEmploi) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.capacite = capacite;
        this.idEmploi = idEmploi;
    }

    public Classe(String nom, String type, int capacite, int idEmploi) {
        this.nom = nom;
        this.type = type;
        this.capacite = capacite;
        this.idEmploi = idEmploi;
    }

   
    public int getId() {
        return id;
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public int getIdEmploi() {
        return idEmploi;
    }

    @Override
    public String toString() {
        return "Classe{" + "id=" + id + ", nom=" + nom + ", type=" + type + ", capacite=" + capacite + ", idEmploi=" + idEmploi + '}';
    }

  
    
}
