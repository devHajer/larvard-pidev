/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;

/**
 *
 * @author ivaxs
 */
public class Epreuve {
    private int idEp ; 
    private String dateEp ; 
    private int duree ; 
    private int niveau ; 
    private String type ;  
    private String reference ; 

    public Epreuve(String dateEp, int duree, int niveau, String type, String reference) {
        this.dateEp = dateEp;
        this.duree = duree;
        this.niveau = niveau;
        this.type = type;
        this.reference = reference;
    }

    public Epreuve(int idEp, String dateEp, int duree, int niveau, String type, String reference) {
        this.idEp = idEp;
        this.dateEp = dateEp;
        this.duree = duree;
        this.niveau = niveau;
        this.type = type;
        this.reference = reference;
    }

    public Epreuve() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdEp() {
        return idEp;
    }

    public void setIdEp(int idEp) {
        this.idEp = idEp;
    }

    public String getDateEp() {
        return dateEp;
    }

    public void setDateEp(String dateEp) {
        this.dateEp = dateEp;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        return "Epreuve{" + "idEp=" + idEp + ", dateEp=" + dateEp + ", duree=" + duree + ", niveau=" + niveau + ", type=" + type + ", reference=" + reference + '}';
    }
    
    
}
