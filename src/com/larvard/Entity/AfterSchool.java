/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Entity;
import com.larvard.Entity.Activite ;
import java.sql.Date;
/**
 *
 * @author ivaxs
 */
public class AfterSchool {
        private int IDAfterschool ;
    private int Activite ; 
       
    private Date DateSchool ;

    public AfterSchool(int IDAfterschool, int Activite, Date DateSchool) {
        this.IDAfterschool = IDAfterschool;
        this.Activite = Activite;
        this.DateSchool = DateSchool;
    }

    public AfterSchool() {
    }

    public AfterSchool(int Activite, Date DateSchool) {
        this.Activite = Activite;
        this.DateSchool = DateSchool;
    }

    public int getIDAfterschool() {
        return IDAfterschool;
    }

    public void setIDAfterschool(int IDAfterschool) {
        this.IDAfterschool = IDAfterschool;
    }

    public int getActivite() {
        return Activite;
    }

    public void setActivite(int Activite) {
        this.Activite = Activite;
    }

    public Date getDateSchool() {
        return DateSchool;
    }

    public void setDateSchool(Date DateSchool) {
        this.DateSchool = DateSchool;
    }

    @Override
    public String toString() {
        return "AfterSchool{" + "IDAfterschool=" + IDAfterschool + ", Activite=" + Activite + ", DateSchool=" + DateSchool + '}';
    }



   

   
}
