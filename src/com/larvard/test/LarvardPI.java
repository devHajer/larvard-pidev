package com.larvard.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author mouhamed
 */
public class LarvardPI extends Application {

    private Stage stage;
    private static LarvardPI instance;
    private Scene scene;

    public LarvardPI() throws IOException, InterruptedException {
        instance = this;
        //System.out.println("eeee");
             //   System.out.println(list);

                  // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/updateProduit.fxml")));

      //scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherProduit.fxml")));
            //  scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AjouterProduit.fxml")));
              // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/cartpage.fxml")));
 // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficheProduitFront.fxml")));
      //   scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/AfficherCommandeFXML.fxml")));
    // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/Quickveiw.fxml")));
 // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/stat.fxml")));
 // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/Quickview.fxml")));
     //scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendParent.fxml")));
          //  scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/BackendEnseignant.fxml")));
    // scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/Quickveiw.fxml")));

  scene = new Scene(FXMLLoader.load(getClass().getResource("/com/larvard/Gui/SignIn.fxml")));

       // System.out.println("");
    }

    public static LarvardPI getInstance() {
        return instance;
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;

        stage.setScene(this.scene);
        stage.initStyle(StageStyle.DECORATED);// reduire et fermer decoredet* - 
        stage.centerOnScreen();
        stage.show();
    }

    public void changescene(Scene scene) {
        this.scene = scene;
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
