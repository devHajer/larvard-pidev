/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Service;

import com.larvard.Entity.Commande;
import com.larvard.Entity.commande_product;
import com.larvard.Entity.Product;
import com.larvard.Entity.User;
import com.larvard.IService.IServiceCommande;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author mouhamed
 */
public class ServiceCommande implements IServiceCommande<Commande> {

    private Connection con;
    private Statement ste;

    public ServiceCommande() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void AjoutCommande(Commande commande) throws SQLException {
        try {

            String query = "INSERT INTO `commande`(`id`,`datecom`,`prixtotale`,`valide`) values (?,?,?,?)";
            PreparedStatement ps;
            ps = DataBase.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, commande.getUser().getId());
            ps.setDate(2, commande.getDatecom());
            ps.setDouble(3, commande.getPrixtotale());
            ps.setString(4, commande.getValide());
            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public boolean deleteCommande(int id) throws SQLException {
        String requete = "DELETE from commande where ID=?";

        PreparedStatement st = con.prepareStatement(requete);
        st.setInt(1, id);
        st.executeUpdate();
        System.out.println("Commande supprimer.");
        return true;
    }

    @Override
    public boolean updateCommande(int id) throws SQLException {
        String requete = "UPDATE commande set prixtotale=? WHERE ID='" + id + "'";

        PreparedStatement st = con.prepareStatement(requete);
        st.setString(1, "5");
        st.executeUpdate();
        System.out.println(" Commande changee");
        return true;
    }

    public User findbyid(int id) {
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM fos_user where id='" + id + "'");
            while (rs.next()) {
                User cc = new User();
                cc.setId(rs.getInt("id"));
                cc.setNom(rs.getString("nom"));
                return cc;
            }

        } catch (SQLException e) {

        }
        return null;
    }

    public Product findproduitbyid(int id) {
        try {

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM product where id='" + id + "'");
            while (rs.next()) {
                Product pp = new Product();
                pp.setId(rs.getInt("id"));
                pp.setPrice(rs.getInt("price"));
                pp.setCategorie(rs.getString("categorie"));
                pp.setGenre(rs.getString("genre"));
                pp.setDescription(rs.getString("description"));
                pp.setImg(rs.getString("img"));
                pp.setAge(rs.getInt("age"));
                pp.setRating(rs.getInt("rating"));
                pp.setStock(rs.getInt("stock"));

                return pp;
            }

        } catch (SQLException e) {

        }
        return null;
    }

    public List<commande_product> getCommandeRowLists(int idcommande) {
        List<commande_product> result = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM commande_product where commande_id='" + idcommande + "'");
            while (rs.next()) {
                commande_product p = new commande_product();
                p.setCommande(getCommandeById(rs.getInt("commande_id")));
                p.setProduct(findproduitbyid(rs.getInt("produit")));

                result.add(p);
            }
            return result;
        } catch (SQLException e) {
        }
        return null;
    }

    public Commande getCommandeById(int id) {
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM commande where id='" + id + "'");
            while (rs.next()) {
                Commande c = new Commande();
                c.setId(rs.getInt("id"));
                c.setUser(findbyid(rs.getInt("id")));
                c.setDatecom(rs.getDate("datecom"));
                c.setValide(rs.getString("valide"));
                c.setPrixtotale(rs.getDouble("prixTotale"));
                return c;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    @Override
    public List<Commande> readAllCommande() throws SQLException {
        List<Commande> arr = new ArrayList<>();

        ste = con.createStatement();

        // ResultSet rs = ste.executeQuery("select * from commande");
     /*   ResultSet rs = ste.executeQuery("SELECT u.username,p.nom,c.prixtotale,c.datecom FROM fos_user u JOIN commande c ON ( u.id = c.id) JOIN product p ON ( c.produit = p.id )");
        while (rs.next()) {

            Commande cmd = new Commande();

            int idp = rs.getInt(2);
            Date dat = rs.getDate(2);
            int user = rs.getInt(1);
            String valide = rs.getString(4);
            double prixtotale = rs.getInt(3);
            User c = findbyid(user);
            String nomuser = c.getUsername();
            System.out.println(nomuser);
            //md.setUser( ).getNomUser();
            Product p = findproduitbyid(idp);
            cmd.getId();
            cmd.setIdproduit(p);
            cmd.setPrixtotale(prixtotale);
            cmd.setDatecom(dat);
            cmd.setValide(valide);
            arr.add(cmd);*/
         Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM commande");
            while (rs.next()) {
                Commande c = new Commande();
              //  c.setId(rs.getInt("id"));
                c.setUser(findbyid(rs.getInt("id")));
                c.setDatecom(rs.getDate("datecom"));
                c.setValide(rs.getString("valide"));
                c.setPrixtotale(rs.getDouble("prixTotale"));
             arr.add(c);
            }
        
        return arr;
    }

    public void valide_btn(Commande a) throws SQLException {
        String query = "update commande set valide=? where id=? ";
        PreparedStatement ps;
        ps = DataBase.getInstance().getConnection().prepareStatement(query);

        ps.setString(1,"valide");
        ps.setInt(2, a.getId());

        ps.executeUpdate();

    }

    public static java.util.Date convertirDate(int d) {
        Timestamp stamp = new Timestamp(d);
        java.util.Date date = new java.util.Date(stamp.getTime());
        return (date);
    }

    @Override
    public void AjoutCommandeproduit(int c, int p) throws SQLException {
               try {

            String query = "INSERT INTO `commande_product` (`commande_id`, `product_id`) VALUES ('?', '?');";
            PreparedStatement ps;
            ps = DataBase.getInstance().getConnection().prepareStatement(query);
     
            ps.setInt(1,c);
            ps.setInt(0,p);

            ps.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        
        
    }
    

}
