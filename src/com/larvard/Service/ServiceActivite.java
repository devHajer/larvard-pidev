/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Activite;
import com.larvard.IService.IServiceActivite; 
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.omg.PortableServer.IdAssignmentPolicy;

/**
 *
 * @author ivaxs
 */
public class ServiceActivite implements IServiceActivite<Activite> {
      private Connection con;
    private Statement ste;

    public ServiceActivite() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
     public void ajouter(Activite act) throws SQLException{
           PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`activite` ( `IDactivite`,`image`, `titre`, `descriptionAct`,`duree`,`type`) VALUES ( NULL,NULL,?, ?, ?,?);");
    pre.setString(1,act.getTitre());
    pre.setString(2,act.getDescription());
    pre.setInt(3,act.getDuree());
     pre.setString(4,act.getType());
    pre.executeUpdate();
    }

    @Override
    public void delete(int t) throws SQLException {
            String requete = "DELETE from activite where IDactivite=?"; 
             //String requete = "DELETE from activite ,afterschool using activite INNER JOIN afterschool WHERE IDactivite=? ";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("Activite Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    } 
        @Override
    public boolean supp(Activite a) throws SQLException {
      PreparedStatement pre;
        pre = con.prepareStatement("DELETE FROM larvard.`activite` WHERE IDactivite= ?;");
        //String requeteDelete = "DELETE FROM pi_dev.`evenement` WHERE nom= ?";
         pre.setInt(1,a.getIdActivite());
         pre.executeUpdate();
        //ste.executeUpdate(requeteDelete);
        return true;  
    
    }

    @Override
    public void updateDescription(int id ,String desc) throws SQLException {
           String requete = "UPDATE activite set descriptionAct=? WHERE IDActivite='"+id+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setString(1,desc);
            st.executeUpdate();
            System.out.println("description modifie avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
       
    }

@Override
    public List<Activite> readAll() throws SQLException {
            List<Activite> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("SELECT * FROM `activite` ORDER BY `activite`.`duree` ASC");
     while (rs.next()) {                
               int IDActivite=rs.getInt(1);
                String titre=rs.getString(2);
               String description=rs.getString("descriptionAct");
               int duree = rs.getInt("duree") ; 
               String type=rs.getString("type");
              Activite act=new Activite(IDActivite,titre, description, duree, type);
               
     arr.add(act);
     }
    return arr;
 }
    @Override 
    public List<String> readTitre() {
            List<String> arr=new ArrayList<>();
            try{
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from activite");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                String titre=rs.getString("titre");
              // String description=rs.getString("descriptionAct");
               //int duree = rs.getInt("duree") ; 
              // String type=rs.getString("type");
              //Activite act=new Activite(titre);
               
     arr.add(titre);
     }
            }catch(SQLException ex){
                
            }
    return arr;
    }
    @Override
    public String getTitre(int i) {
           String titre="";
            try{
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from activite where IDactivite='"+i+"'");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
               titre= rs.getString("titre");
            
              // String description=rs.getString("descriptionAct");
               //int duree = rs.getInt("duree") ; 
              // String type=rs.getString("type");
              //Activite act=new Activite(titre);
               
    
     }
            }catch(SQLException ex){
                
            }
    return titre;
    }
    
    public int searchactbytitle (String titre) {
           int id = 0;
        try {
            Statement ps=con.createStatement();
            ResultSet res;
           
            res=ps.executeQuery("select * from activite where titre like '%"+titre+"%' ");
            while(res.next())
            {
                 id = res.getInt("IDactivite");
               // String tite=res.getString("titre");
             
              // System.out.println("L'id est "+idcatart+" nom"+NomCatart);      
               // return  new Activite(id, tite);
      
            }
        } catch (SQLException ex) {
            System.out.println(""+ex.getMessage());
        }
        return id;
    }
    }

