/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Emploi;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import com.larvard.utils.DataBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;  
import com.larvard.IService.IServiceEmploi;

/**
 *
 * @author ivaxs
 */
public class ServiceEmploi implements IServiceEmploi<Emploi>{
   private Connection con;
    private Statement ste;

    public ServiceEmploi() {
        con = DataBase.getInstance().getConnection();

    }

    
    @Override
    public void ajouter(Emploi lec) throws SQLException {
          PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`emploi` ( `idEmploi`, `nom`, `description`,`type`,`idEnseignant`) VALUES ( NULL,?, ?, ?,?);");
    pre.setString(1,lec.getNom());
    pre.setString(2,lec.getDescription());
    pre.setString(3,lec.getType());
    pre.setInt(4,lec.getIdEnseignant()) ;
    pre.executeUpdate();
    }

    @Override
    public void delete(int id) throws SQLException {
        
            String requete = "DELETE from emploi where idEmploi=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,id);
            st.executeUpdate();
            System.out.println("emploi Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void update(Emploi t , String nom) throws SQLException {
               String requete = "UPDATE emploi set nom=? WHERE idEmploi='"+t+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setString(1,nom);
            st.executeUpdate();
            System.out.println("nom emploi modifie avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
       
    }

    @Override
    public List<Emploi> readAll() throws SQLException {
                   List<Emploi> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from emploi");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
              String nom=rs.getString("nom"); 
               String description=rs.getString("description"); 
               String type=rs.getString("type");
               int idEnseigant=rs.getInt("idEnseignant");
              Emploi act=new Emploi(nom,description,type,idEnseigant);
               
     arr.add(act);
     }
    return arr;
    }   
}
