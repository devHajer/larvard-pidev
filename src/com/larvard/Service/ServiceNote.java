/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Note ;
import com.larvard.IService.IServiceNote;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ivaxs
 */
public class ServiceNote implements IServiceNote<Note>{

        private Connection con;
    private Statement ste;

    public ServiceNote() {
        con = DataBase.getInstance().getConnection();

    }
    
    
    @Override
    public void ajouter(Note c) throws SQLException {
         PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`Note` ( `idNote`, `mat1`, `mat2`,`mat3`,`mat4`) VALUES ( NULL,?, ?, ?,?);");
    pre.setInt(1,c.getMat1());
    pre.setInt(2,c.getMat2());
    pre.setInt(3,c.getMat3());
     pre.setInt(4,c.getMat4()) ;
    pre.executeUpdate();
          
    }

    @Override
    public void delete(int t) throws SQLException {
        String requete = "DELETE from note where idNote=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("Note Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /* @Override
   public void updateNomColonne(String mati, String mat) throws SQLException {
          	
              String requete = "ALTER TABLE note RENAME COLUMN mat1 TO '"+mat+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
           
             st.setString(1,mat);
            st.executeUpdate();
            System.out.println("colonne modifie avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }*/

    @Override
    public List<Note> readAll() throws SQLException {
              List<Note> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from Note");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                int mat1=rs.getInt(2);
               int mat2=rs.getInt(3);
               int mat3=rs.getInt(5);
                int mat4=rs.getInt(6);
               Note c =new Note(mat1,mat2,mat3,mat4);
                arr.add(c);   
}        
      return arr;
 }    
    
}
