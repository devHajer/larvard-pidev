/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.IService.IServiceClasse;
import com.larvard.Entity.Classe; 
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import com.larvard.utils.DataBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
/**
 *
 * @author ivaxs
 */
public class ServiceClasse  implements IServiceClasse<Classe> {
     private Connection con;
    private Statement ste;

    public ServiceClasse() {
        con = DataBase.getInstance().getConnection();

    }
    
    @Override
    public void ajouter(Classe c) throws SQLException {
     PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`classe` ( `id`, `nom`, `type`,`capacite`,`idEmploi`) VALUES ( NULL,?, ?, ?,?);");
    pre.setString(1,c.getNom());
    pre.setString(2,c.getType());
    pre.setInt(3,c.getCapacite());
     pre.setInt(4,c.getIdEmploi()) ;
    pre.executeUpdate();
    
    }

    @Override
    public void delete(int t) throws SQLException {

            String requete = "DELETE from classe where id=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("Classe Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void updateNomClasse(int id, String nom) throws SQLException {
               String requete = "UPDATE classe set nom=? WHERE id='"+id+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setString(1,nom);
            st.executeUpdate();
            System.out.println("nom modifie avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    

    @Override
    public List<Classe> readAll() throws SQLException {
                 List<Classe> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from classe");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                String nom=rs.getString(2);
               String type=rs.getString("type");
               int capacite=rs.getInt("capacite");
                int idEmploi=rs.getInt("idEmploi");
              
              Classe c=new Classe(nom,type,capacite,idEmploi);
               
     arr.add(c);
     }
    return arr;
}
    public List<Classe> SearchClasseByName(String nom) throws SQLException {
        String requete="SELECT * FROM classe where nom= '"+nom+"'" ;
        ste=con.createStatement() ;
        ResultSet rs=ste.executeQuery(requete);
        List<Classe> list = new ArrayList<>() ; 
        while(rs.next()){
       Classe e;
          e = new Classe(rs.getString("nom"),rs.getString("type"),rs.getInt("capacite"),rs.getInt("idEmploi")) ;
            //e = new Classe(rs.getInt("idevt"),rs.getString("nom"),rs.getString("lieu"),rs.getString("date"));
        list.add(e) ;
        }
        return list ;
    }
    
}
