/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Service;

import com.larvard.Entity.Product;
import com.larvard.IService.IserviceProduct;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mouhamed
 */
public class ServiceProduct implements IserviceProduct<Product> {

    private Connection con;
    private Statement ste;

    public ServiceProduct() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void AjoutProduit(Product pr) throws SQLException {

        try {//               Product p=new Product(id,price,Categorie,genre,description,img,age,rating,stock);

            String query = "INSERT INTO `product` ( `price`,`Nom`,`Categorie`,`genre`,`description`,`img`,`rating`,`age`,`stock`,`quantite`) values (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps;

            ps = DataBase.getInstance().getConnection().prepareStatement(query);
            ps.setDouble(1, pr.getPrice());
            ps.setString(2, pr.getNom());
            ps.setString(3, pr.getCategorie());
            ps.setString(4, pr.getGenre());
            ps.setString(5, pr.getDescription());
            ps.setString(6, pr.getImg());
            ps.setInt(7, pr.getAge());
            ps.setInt(8, 2);
            ps.setInt(9, pr.getStock());
            ps.setInt(10, pr.getQuantite());
            ps.executeUpdate();

        } catch (SQLException ex) {
                        System.out.println(ex);

        }
    }

    @Override
    public boolean deleteProduit(Product p) throws SQLException {
        String requete = "DELETE from product where ID=?";

        PreparedStatement st = con.prepareStatement(requete);
        st.setInt(1, p.getId());
        st.executeUpdate();
        System.out.println("Produit supprime.");
        return true;

    }

    @Override
    public boolean updateProduit(Product b) throws SQLException {
        String reqUp = "update product set categorie=?,stock=?,age=?,description=? where id=?";
        PreparedStatement pss = DataBase.getInstance().getConnection().prepareStatement(reqUp);

        System.out.println(b);
        pss.setString(1, b.getCategorie());

        pss.setInt(2, b.getStock());
        pss.setInt(3, b.getAge());
        pss.setString(4, b.getDescription());

        pss.setInt(5, b.getId());

        pss.executeUpdate();
        return false;
    }

    @Override
    public List<Product> readAllProduit() throws SQLException {
        List<Product> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from product");
        while (rs.next()) {
            int id = rs.getInt(1);
            String nom =rs.getString(2);
            int price = rs.getInt(3);
            String Categorie = rs.getString(4);
            String genre = rs.getString(5);
            String description = rs.getString(6);
            String img = rs.getString(7);
            int age = rs.getInt(8);
            int rating = rs.getInt(9);
            int stock = rs.getInt(10);
            int quantite = rs.getInt(11);
            Product p = new Product(id,nom, price, Categorie, genre, description, img, age, rating, stock, quantite);
            arr.add(p);
        }
        return arr;
    }

    public boolean searchbyname(String firstname) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Product> searchProductByID(String categorie) throws SQLException {
        String requete = "SELECT * FROM Product p where p.ID in (Select id from Product pp where pp.categorie='" + categorie + "')";
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery(requete);
        List<Product> list = new ArrayList<>();
        while (rs.next()) {

            Product p = new Product(rs.getInt("id"),rs.getString("nom"),
                    rs.getDouble("price"),
                    rs.getString("Categorie"),
                    rs.getString("genre"), rs.getString("description"), rs.getString("img"),
                    rs.getInt("age"), rs.getInt("rating"), rs.getInt("stock"), rs.getInt("quantite"));
            list.add(p);
        }
        return list;
    }

    public List<Product> AfficherProduit() {
        List ALLproducts = new ArrayList();
        try {
            String query = "select * from product WHERE stock > 0 OR stock=null";
            ste = con.createStatement();
            ResultSet rest = ste.executeQuery(query);
            while (rest.next()) {
                Product pr = new Product();

                pr.setId(rest.getInt("id"));            
                pr.setNom(rest.getString("nom"));
                pr.setPrice(rest.getInt("price"));
                pr.setRating(rest.getInt("rating"));
                pr.setStock(rest.getInt("stock"));
                pr.setQuantite(rest.getInt("quantite"));
                pr.setImg(rest.getString("img"));
                pr.setCategorie(rest.getString("Categorie"));
                pr.setDescription(rest.getString("description"));
                pr.setAge(rest.getInt("age"));
                pr.setGenre(rest.getString("genre"));
                ALLproducts.add(pr);

            }

        } catch (SQLException ex) {
            System.out.println("ex");
        }
        System.out.println("tout les produit *****  " + ALLproducts);

        return ALLproducts;

    }

    public List<Product> RechercheProduitParNom(String recherche) {

        List ALLproducts = new ArrayList();
        try {
            String query = "select * from product WHERE nom LIKE '%" + recherche + "%' AND stock > 0 OR stock=null;";
            ste = con.createStatement();
            ResultSet rest = ste.executeQuery(query);
            while (rest.next()) {
                Product pr = new Product();

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("Nom"));
                pr.setPrice(rest.getDouble("price"));
                pr.setStock(rest.getInt("stock"));
                pr.setQuantite(rest.getInt("quantite"));
                pr.setImg(rest.getString("img"));
                pr.setCategorie(rest.getString("Categorie"));
                pr.setDescription(rest.getString("description"));
                pr.setAge(rest.getInt("age"));
                pr.setGenre(rest.getString("genre"));
                ALLproducts.add(pr);
            }

        } catch (SQLException ex) {            System.out.println("ex");

        }

        return ALLproducts;

    }

    public void UpdateProduitStock(Product b) throws SQLException {
        //-------------------- Update ----------//

        String reqUp = "update product set stock=? where id=?";
        PreparedStatement st = con.prepareStatement(reqUp);

        st.setInt(1, b.getStock());
        st.setInt(2, b.getId());
        st.executeUpdate();

    }

    public Product GetProduitbyid(int b) throws SQLException {
        //-------------------- Update ----------//
        Product pr = new Product();

        String query = "select * from product where id = ? ";
        PreparedStatement ps;
        try {
            ps = DataBase.getInstance().getConnection().prepareCall(query);
            ps.setInt(1, b);
            ResultSet rest = ps.executeQuery();

            while (rest.next()) {

                pr.setId(rest.getInt("id"));
                pr.setNom(rest.getString("nom"));
                pr.setPrice(rest.getDouble("price"));
                pr.setStock(rest.getInt("stock"));
                pr.setQuantite(rest.getInt("quantite"));
                pr.setImg(rest.getString("img"));
                pr.setCategorie(rest.getString("categorie"));
                pr.setDescription(rest.getString("description"));
                pr.setAge(rest.getInt("age"));
                pr.setGenre(rest.getString("genre"));
            }

        } catch (SQLException ex) {            System.out.println("ex");

        }
        return pr;

    }
        public void AjouterRating(int r, int id, String nomP)
    {
      
        String query2="insert into rating (rating,nom_produit,idproduit) values (?,?,?)";
        PreparedStatement ps2;
        try {
            ps2 = DataBase.getInstance().getConnection().prepareStatement(query2);
           
            ps2.setInt(1,r);
            ps2.setString(2,nomP);
            ps2.setInt(3,id);
           
        ps2.executeUpdate();

        } catch (SQLException ex) {            System.out.println("ex");

            
        }
    }
      
    public int AfficherRating(int id) throws SQLException
    {  int rate = 0;
        
            Connection conn= (Connection) DataBase.getInstance().getConnection();
            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery("select avg(rating) from rating where idproduit="+id);
   
        while (rs.next()) {
            rate= rs.getInt(1);
            
        }
            return rate;
        }
    

}
