/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Service;

import com.larvard.Entity.Enfant;
//import com.larvard.Entity.Enseignant;
import com.larvard.Entity.Epreuve;
import com.larvard.Entity.Evaluation;
//import com.larvard.Entity.Parent;
import com.larvard.Entity.User;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oussema
 */
public class EvaluationService {

    private Connection cnx;
    private Statement ste;

    public EvaluationService() {
         cnx = DataBase.getInstance().getConnection();

    } 
    public void addEvaluation(Evaluation e) {
        String sql = "Insert into evaluation (decision,appreciation,idUser,IdEpreuve,IDEnfant) "
                + "VALUES  (?,?,?,?,?)";
        try {
            PreparedStatement pr = DataBase.getInstance().getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pr.setInt(1, e.getDecision());
            pr.setString(2, e.getAppreciation());
           pr.setInt(3, e.getIdEnseignant());
            pr.setInt(4, e.getIdepreuve());
            pr.setInt(5, e.getIdenfant());
            pr.execute();
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateEvaluation(Evaluation e) {
        String sql = "update evaluation"
                + " set decision = ?,appreciation = ?, idEpreuve = ?, IDEenfant = ?"
                + " where idEvaluation  = ?";
        try {
            PreparedStatement pr = DataBase.getInstance().getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pr.setInt(1, e.getDecision());
            pr.setString(2, e.getAppreciation());
            pr.setInt(3, e.getIdepreuve());
            pr.setInt(4, e.getIdenfant());
            pr.setInt(5, e.getId());
            pr.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean deleteEvaluation(int id) {
        String sql = "DELETE FROM evaluation WHERE idEvaluation=" + id;
        Statement s;
        try {
            s = DataBase.getInstance().getConnection().createStatement();
            s.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public ArrayList<Evaluation> getAllEvaluations() {
        String sql = "select * from evaluation";
        ArrayList<Evaluation> evaluationList = new ArrayList<>();

        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Evaluation e = this.getEvaluationById(rs.getInt("idEvaluation"));
                evaluationList.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return evaluationList;
    }

    public Evaluation getEvaluationById(int id) {
        String sql = "Select * from evaluation where idEvaluation=" + id;
        Evaluation c = new Evaluation();
        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                c.setId(id);
                c.setDecision(rs.getInt("decision"));
                c.setAppreciation(rs.getString("appreciation"));
//                c.setIdenfant(this.getEnfantById(rs.getInt("id_enfant")).getIDEnfant());
//                c.setIdepreuve(this.getEpreuveById(rs.getInt("idEp")).getIdEp());
//                c.setIduser(this.getUserById(rs.getInt("idEnseignant")).getId());
                
                c.setIdenfant(rs.getInt("IDEnfant"));
                c.setIdepreuve(rs.getInt("idEpreuve"));
                c.setIduser(rs.getInt("idUser"));                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;
    }

    public Enfant getEnfantById(int id) {
        
        String sql = "SELECT * FROM `enfant` WHERE IDEnfant=" + id;
        Enfant c = new Enfant();
        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
             
            while (rs.next()) {
                
                c.setIDEnfant(id);
                c.setNom(rs.getString("nom"));
                c.setPrenom(rs.getString("prenom"));
                c.setDateNaissance(rs.getDate("DateNaissance"));                
//                c.setParent(this.getParentById(rs.getInt("id_parent")));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;
    }

    public Epreuve getEpreuveById(int id) {
        String sql = "SELECT * FROM `epreuve` WHERE id= " + id;
        Epreuve c = new Epreuve();
        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                c.setIdEp(id);
                c.setDateEp(rs.getString("dateEp"));
                c.setDuree(rs.getInt("duree"));
                c.setNiveau(rs.getInt("niveau"));
                c.setReference(rs.getString("reference"));
                c.setType(rs.getString("type"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;    
    }

    public User getUserById(int id) {
                String sql = "SELECT * FROM `user` WHERE id= " + id;
        User c = new User();
        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                c.setId(id);
               c.setEmail(rs.getString("email"));
               c.setNom(rs.getString("nomuser"));
               c.setPrenom(rs.getString("prenomuser"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;   
    }

/*    public Parent getParentById(int id) {
                        String sql = "SELECT * FROM `Parent` WHERE id= " + id;
        Parent c = new Parent();
        try {
            Statement s = cnx.getCnx().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
               c.setId(id);
               
               c.setNom(rs.getString("nom"));
               c.setEmail(rs.getString("mail"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return c;
    }
*/    
    
    public ArrayList<Enfant> getAllEnfants(){
        String sql="select * from enfant";
        ArrayList<Enfant> enfantList = new ArrayList<>();

        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Enfant e = this.getEnfantById(rs.getInt("IDEnfant"));
                enfantList.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return enfantList;
    }
    
    
        public ArrayList<Evaluation> getEpreuveByAll(String value) {
        String sql = "select * from evaluation where "
                 + "appreciation like '%" +value + "%'" ;

        ArrayList<Evaluation> evaluationList = new ArrayList<>();

        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Evaluation e = this.getEvaluationById(rs.getInt("idEvaluation"));
                evaluationList.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return evaluationList;
    }
        
        public ArrayList<Enfant> getEnfantsByUser(int id) {
             
        String sql = "SELECT * FROM `enfant` WHERE idUser = " + id;
//        String sql="SELECT * FROM `enfant` WHERE idUser=1";
        ArrayList<Enfant> enfantList = new ArrayList<>();

        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            
            while (rs.next()) {
//                Enfant c = new Enfant();
                
//                c.setIDEnfant(id);
//                c.setNom(rs.getString("nom"));
//                c.setPrenom(rs.getString("prenom"));
                Enfant e = this.getEnfantById(rs.getInt("IDEnfant"));
                
                enfantList.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return enfantList;

    }
    public ArrayList<Evaluation> getEvaluationByEnfant(int id){
        String sql= "Select * from evaluation where IDEnfant = "+id;
        ArrayList<Evaluation> evaluationList = new ArrayList<>();

        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Evaluation e = this.getEvaluationById(rs.getInt("idEvaluation"));
                evaluationList.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvaluationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return evaluationList;
    }
        public int successTestByEnfant(int id) {
        String sql = "Select count(*) from evaluation where IDEnfant = " + id + " and decision > 5";
        try {
            Statement s =DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(EpreuveService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
        public int failTestByEnfant(int id) {
        String sql = "Select count(*) from evaluation where IDEnfant = " + id + " and decision < 6";
        try {
            Statement s =DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(EpreuveService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    public double successRateByEnfant(int id ){
        int successNumber = successTestByEnfant(id);
        int failNumber = failTestByEnfant(id);
        return (  (((double)successNumber/(failNumber+successNumber)))*100);
    }
}
