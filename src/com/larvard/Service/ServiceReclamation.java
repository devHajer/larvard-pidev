/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;

import com.larvard.Entity.Reclamation;
import com.larvard.IService.IServiceReclamation;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class ServiceReclamation implements IServiceReclamation<Reclamation>  {
       private  Connection con;
    private Statement ste;
    
     public ServiceReclamation() {
        con = DataBase.getInstance().getConnection();

    }
    
    ;

    @Override 
    public void ajouter(Reclamation rec) throws SQLException ,ParseException {
        SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
   java.util.Date myDate = format.parse( rec.getDate() );  // Notice the ".util." of package name.
        PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`Reclamation` ( `id`, `id_parent`, `DateAjout`,`type`,`sujet`,`etat`) VALUES ( NULL,?, ?, ?,?,?);");
    pre.setInt(1,rec.getId_parent());
    java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name
    pre.setDate( 2, sqlDate );
    pre.setString(3,rec.getType());
         pre.setString(4,rec.getSujet());
     pre.setBoolean(5,rec.isEtat());

    pre.executeUpdate();
    }

    @Override
    public void delete(int id) throws SQLException {
          String requete = "DELETE from Reclamation where id=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,id);
            st.executeUpdate();
            System.out.println("reclamation Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void updatetat(int id, boolean etat) throws SQLException {
        
                 String requete = "UPDATE Reclamation set etat=? WHERE id='"+id+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setBoolean(1,etat);
            st.executeUpdate();
            System.out.println("etat reclamation  change");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public List<Reclamation> readAll() throws SQLException {
         List<Reclamation> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from Reclamation");
     while (rs.next()) {                
               int id=rs.getInt(1);
               int id_parent=rs.getInt(2);
               String date=rs.getString("DateAjout");
               String type=rs.getString("type");
               boolean etat=rs.getBoolean("etat");
               String sujet=rs.getString("sujet");
               Reclamation r = new Reclamation(id,id_parent,date,type,etat,sujet);
               
              
     arr.add(r);
     }
    return arr;
    }
    
    
}
