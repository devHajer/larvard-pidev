/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Lecon ;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import com.larvard.utils.DataBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;  
import com.larvard.IService.IServiceEmploi;
import com.larvard.IService.IServiceLecon;
import java.text.SimpleDateFormat;
import java.text.ParseException ;
/**
 *
 * @author ivaxs
 */
public class ServiceLecon  implements IServiceLecon<Lecon>{
    
            private Connection con;
    private Statement ste;

    public ServiceLecon() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void ajouter(Lecon lec) throws SQLException , ParseException {
        SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
java.util.Date myDate = format.parse(lec.getDateLecon());  // Notice the ".util." of package name.
     PreparedStatement pre=con.prepareStatement("INSERT INTO `larvard`.`lecon` ( `idLec`, `nom`, `description`,`DateLecon`,`duree`,`idEmploi`,`idEnseignant`,`idSalle`) VALUES ( NULL,?, ?, ?,?,?,?,?);");
    pre.setString(1,lec.getNom());
    pre.setString(2,lec.getDescription());
     java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
    pre.setDate(3 , sqlDate );
    pre.setInt(4,lec.getDuree()) ;
     pre.setInt(5,lec.getIdEmploi()) ;
      pre.setInt(6,lec.getIdEnseignant()) ;
       pre.setInt(7,lec.getIdSalle()) ;
    pre.executeUpdate();    }

    @Override
    public void delete(int id) throws SQLException {
             String requete = "DELETE from lecon where idLec=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,id);
            st.executeUpdate();
            System.out.println("lecon Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void update(Lecon t , String nom) throws SQLException {
                  String requete = "UPDATE lecon set nom=? WHERE idLec='"+t+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setString(1,nom);
            st.executeUpdate();
            System.out.println("nom lecon modifie avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public List<Lecon> readAll() throws SQLException {
                         List<Lecon> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from lecon");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
              String nom=rs.getString("nom"); 
               String description=rs.getString("description");
               String DateLecon=rs.getString("DateLecon");
               int duree=rs.getInt("duree");
               int idEmploi=rs.getInt("idEmploi");
               int idEnseignant=rs.getInt("idEnseignant");
               int idSalle=rs.getInt("idSalle");
              Lecon lec=new Lecon(nom,description,DateLecon,duree,idEmploi,idEnseignant,idSalle);
               
     arr.add(lec);
     }
    return arr;
    }
    
}
