/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.larvard.Service;

import com.larvard.Entity.Epreuve;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oussema
 */
public class EpreuveService {
    private Connection cnx;
    private Statement ste;

    public EpreuveService() {
         cnx = DataBase.getInstance().getConnection();

    }        
    public void ajouter(Epreuve ep) throws SQLException, ParseException {
        SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  
        java.util.Date myDate = format.parse( ep.getDateEp());
        PreparedStatement pre=DataBase.getInstance().getConnection().prepareStatement("INSERT INTO epreuve ( `dateEp`, `duree`,`niveau`,`type`,`reference`) VALUES ( ?,?,?,?,?);");
   
        java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() );
        pre.setDate( 1, sqlDate ); 
        pre.setInt(2,ep.getDuree());
        pre.setInt(3,ep.getNiveau());
        pre.setString(4,ep.getType());
        pre.setString(5,ep.getReference());
        pre.executeUpdate();
    }
    public void delete(int t) throws SQLException {
            String requete = "DELETE from epreuve where id=?";
            try {
            PreparedStatement st =DataBase.getInstance().getConnection().prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("Epreuve Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
        public void updateEp(Epreuve e) throws SQLException {
                  String requete = "UPDATE epreuve set type=? ,duree=?,niveau=?,reference=? WHERE id='"+e.getIdEp()+"'";
        try {
            PreparedStatement st = DataBase.getInstance().getConnection().prepareStatement(requete);
            st.setString(1,e.getType());
            st.setInt(2, e.getDuree());
            st.setInt(3, e.getNiveau());
            st.setString(4, e.getReference());
            st.executeUpdate();
            System.out.println("date de l epreuve avec succes");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
       
    }
    public List<Epreuve> readAll() throws SQLException {
               List<Epreuve> arr=new ArrayList<>();
    
    ResultSet rs=DataBase.getInstance().getConnection().createStatement().executeQuery("select * from epreuve");
     while (rs.next()) {                
               
               String dateEp=rs.getString("dateEp");
               int id= rs.getInt("id") ; 
               int duree= rs.getInt("duree") ; 
               int niveau=rs.getInt("niveau");
               String type=rs.getString("type");
               String reference=rs.getString("reference");
              
              Epreuve act=new Epreuve(id,dateEp,duree,niveau,type,reference);       
     arr.add(act);
     }
    return arr;
    }    
    public Epreuve getEpreuveById(int id){
        String sql="Select * from epreuve where id="+id;
        Epreuve act =new Epreuve();
        try {
            Statement s = DataBase.getInstance().getConnection().createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
               String dateEp=rs.getString("dateEp");
               int ide= rs.getInt("id") ; 
               int duree= rs.getInt("duree") ; 
               int niveau=rs.getInt("niveau");
               String type=rs.getString("type");
               String reference=rs.getString("reference");
               act=new Epreuve(ide,dateEp,duree,niveau,type,reference);       

            }
        } catch (SQLException ex) {
        }
        
        return act;
    }
        
     public List<Epreuve> getEpreuveByAll(String value){
        ResultSet rs;
        List<Epreuve> arr=new ArrayList<>();
        try {
            rs = DataBase.getInstance().getConnection().createStatement().executeQuery(
                    "Select * from epreuve where "
                            + "duree like '%" + value +  "%'  "
                                    + "or niveau like '%" + value +  "%'  "
                                            + "or type like '%" + value +  "%'  "
                                                    + "or reference like '%" + value +  "%'  ");
       
        
        while (rs.next()) {                
               
               String dateEp=rs.getString("dateEp");
               int id= rs.getInt("id") ; 
               int duree= rs.getInt("duree") ; 
               int niveau=rs.getInt("niveau");
               String type=rs.getString("type");
               String reference=rs.getString("reference");
              
              Epreuve act=new Epreuve(id,dateEp,duree,niveau,type,reference);       
        arr.add(act);
        }
       return arr;
      } catch (SQLException ex) {
            Logger.getLogger(EpreuveService.class.getName()).log(Level.SEVERE, null, ex);
        }
   return arr;
    }

}
