/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Classe;
import com.larvard.Entity.Enfant;
import com.larvard.IService.IServiceEnfant;
import com.larvard.utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement ;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ivaxs
 */
public class ServiceEnfant implements IServiceEnfant<Enfant>{
        private Connection con;
    private Statement ste;

    public ServiceEnfant() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void ajouter(Enfant enf) throws SQLException , ParseException{
   PreparedStatement pre=con.prepareStatement("INSERT INTO `pidev`.`enfant` ( `id`, `nom`, `prenom`,`DateNaissance`,`image`,`IdClasse`,`IDAfterSchool`,`idUser`) VALUES ( NULL,?,?,?,?,?,?,NULL);");
      SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
java.util.Date myDate;  // Notice the ".util." of package name. 
           // myDate = format.parse( enf.getDateNaissance());
    pre.setString(1,enf.getNom());
    pre.setString(2,enf.getPrenom());
    // java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
    //pre.setDate( 3, sqlDate );
      pre.setString(4,enf.getImage());
       pre.setInt(5,enf.getClasse().getId());
               pre.setInt(6,enf.getAfterSchool().getActivite());
    pre.executeUpdate();
    }

    @Override
    public void delete(int t) throws SQLException {
       
            String requete = "DELETE from enfant where id=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("enfant supprime.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void updateIMG(int t,String img) throws SQLException {
            String requete = "UPDATE enfant set image=? WHERE id='"+t+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setString(1,img);
            st.executeUpdate();
            System.out.println(" image changee");
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public List<Enfant> readAll() {
                List<Enfant> arr=new ArrayList<>();
                try { 
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from enfant");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                
               String nom=rs.getString("nom");
               String prenom= rs.getString("prenom") ; 
               String DateNaissance=rs.getString("DateNaissance");
               String image=rs.getString("image");
               Classe classe=(Classe) rs.getObject("IdClasse");
             int AfterSchool =rs.getInt("IDAfterSchool");
              Enfant act=new Enfant(nom,prenom,DateNaissance,image,classe,AfterSchool );       
     arr.add(act);
     }}catch(SQLException e){System.out.println(e);}
    return arr;
    }
    @Override
    public List<Enfant> readnomprdate() {
                List<Enfant> arr=new ArrayList<>();
                try{
    ste=con.createStatement();
    
    ResultSet rs=ste.executeQuery("select * from enfant");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                int id=rs.getInt("id");
               String nom=rs.getString("nom");
               String prenom= rs.getString("prenom") ; 
               String DateNaissance=rs.getString("DateNaissance"); 
               int af =rs.getInt("IDAfterschool");
              Enfant en=new Enfant(id,nom,prenom,DateNaissance,af);       
     arr.add(en);
     }} catch(SQLException e){System.out.println(e);}
    return arr;
    }
    @Override
    public void updateAfterSchool(int ide, int ida){
         String requete = "UPDATE enfant set IdAfterschool=? WHERE id='"+ide+"'";
        try {
            PreparedStatement st = con.prepareStatement(requete);
            st.setInt(1,ida);
            st.executeUpdate();
            System.out.println("id modifie avec succes");
           
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
