/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;
import com.larvard.Entity.Evenement;
import com.larvard.IService.IServiceEvenement;
import java.sql.SQLException;
import java.util.List;
import java.sql.*;
import com.larvard.utils.DataBase;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ivaxs
 */
public class ServiceEvenement implements IServiceEvenement<Evenement>{
      private final Connection con;
    private Statement ste;

    public ServiceEvenement() {
        con = DataBase.getInstance().getConnection();

    }
    
    /**
     *
     * @param e
     * @throws SQLException
     * @throws ParseException
     */
    @Override
    public void ajouter(Evenement e) throws SQLException, ParseException
    {
        SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
java.util.Date myDate = format.parse( e.getDate() );  // Notice the ".util." of package name.
    PreparedStatement pre;
        pre = con.prepareStatement("INSERT INTO `larvard`.`event` (`local`, `date`,`nbpart`,`nom`) VALUES ( ?, ?, NULL, ?);");
    pre.setString(1, e.getLieu());
    pre.setString(3, e.getNom());
    java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
    pre.setDate( 2, sqlDate );
    pre.executeUpdate();
//    SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
//java.util.Date myDate = format.parse( "10/10/2009" );  // Notice the ".util." of package name.
//
//PreparedStatement pstmt = connection.prepareStatement(
//"INSERT INTO USERS ( USER_ID, FIRST_NAME, LAST_NAME, SEX, DATE ) " +
//" values (?, ?, ?, ?, ? )");
//
//pstmt.setString( 1, userId );
//pstmt.setString( 3, myUser.getLastName() ); 
//pstmt.setString( 2, myUser.getFirstName() ); // please use "getFir…" instead of "GetFir…", per Java conventions.
//pstmt.setString( 4, myUser.getSex() );
//java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
//pstmt.setDate( 5, sqlDate ); 
    }
//    public void ajouter2(Evenement e) throws SQLException, ParseException
//    {
//        //SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
////java.util.Date myDate = format.parse( e.getDate() );  // Notice the ".util." of package name.
//    PreparedStatement pre;
//        pre = con.prepareStatement("INSERT INTO `larvard`.`evenement` (`lieu`, `date`,`nbpart`,`nom`) VALUES ( ?, ?, NULL, ?);");
//    pre.setString(1, e.getLieu());
//    pre.setString(3, e.getNom());
//    //java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
//    pre.setDate( 2, e.getDates() );
//    pre.executeUpdate();
////    SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
////java.util.Date myDate = format.parse( "10/10/2009" );  // Notice the ".util." of package name.
////
////PreparedStatement pstmt = connection.prepareStatement(
////"INSERT INTO USERS ( USER_ID, FIRST_NAME, LAST_NAME, SEX, DATE ) " +
////" values (?, ?, ?, ?, ? )");
////
////pstmt.setString( 1, userId );
////pstmt.setString( 3, myUser.getLastName() ); 
////pstmt.setString( 2, myUser.getFirstName() ); // please use "getFir…" instead of "GetFir…", per Java conventions.
////pstmt.setString( 4, myUser.getSex() );
////java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
////pstmt.setDate( 5, sqlDate ); 
//    }
            public void add(Evenement e) throws SQLException, ParseException
    {
        //SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
//java.util.Date myDate = format.parse( e.getDate() );  // Notice the ".util." of package name.
    PreparedStatement pre;
        pre = con.prepareStatement("INSERT INTO `larvard`.`event` (`local`, `date`,`nbpart`,`nom`) VALUES ( ?, ?, ?, ?);");
    pre.setString(1, e.getLieu());
    pre.setString(4, e.getNom());
    pre.setTimestamp(2, e.getDatet());
    pre.setInt(3, e.getNbpart());
    //java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
    //pre.setDate( 2, sqlDate );
    pre.executeUpdate();
//    SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
//java.util.Date myDate = format.parse( "10/10/2009" );  // Notice the ".util." of package name.
//
//PreparedStatement pstmt = connection.prepareStatement(
//"INSERT INTO USERS ( USER_ID, FIRST_NAME, LAST_NAME, SEX, DATE ) " +
//" values (?, ?, ?, ?, ? )");
//
//pstmt.setString( 1, userId );
//pstmt.setString( 3, myUser.getLastName() ); 
//pstmt.setString( 2, myUser.getFirstName() ); // please use "getFir…" instead of "GetFir…", per Java conventions.
//pstmt.setString( 4, myUser.getSex() );
//java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
//pstmt.setDate( 5, sqlDate ); 
    }

    //@Override
      @Override
   public boolean delete(Evenement e) throws SQLException {
        //ste = con.createStatement();
        PreparedStatement pre;
        pre = con.prepareStatement("DELETE FROM larvard.`event` WHERE nom= ?;");
        //String requeteDelete = "DELETE FROM pi_dev.`evenement` WHERE nom= ?";
         pre.setString(1, e.getNom());
         pre.executeUpdate();
        //ste.executeUpdate(requeteDelete);
        return true;  
    }
//
    @Override
    public boolean update(Evenement e) throws SQLException {
       //te = con.createStatement();
        //String requeteUpdate = "UPDATE larvard1.`enfant` SET nom='"+t.getNom()+"'"+"WHERE id= " + t.getIdEnft();
        //ste.executeUpdate(requeteUpdate);
        PreparedStatement pre;
        pre = con.prepareStatement("UPDATE larvard.`event` SET local= ? WHERE nom= ?;");
        pre.setString(1, e.getLieu());
         pre.setString(2, e.getNom());
         pre.executeUpdate();
        return true; 
    }

    @Override
    public List<Evenement> readAll() throws SQLException {
    List<Evenement> arr=new ArrayList<>();
    ste=con.createStatement();
   ResultSet rs=ste.executeQuery("select * from event");
     while (rs.next()) {                
               int idevt=rs.getInt(1);
               String nom=rs.getString("nom");
               String date=rs.getString("date");
               String lieu=rs.getString("local");
               Evenement e=new Evenement(idevt, nom, lieu, date);
     arr.add(e);
     }
    return arr;
    }
    @Override
    public List<Evenement> SearchEvenementByName(String nom) throws SQLException {
        String requete="SELECT * FROM event where nom= '"+nom+"'" ;
        ste=con.createStatement() ;
        ResultSet rs=ste.executeQuery(requete);
        List<Evenement> list = new ArrayList<>() ; 
        while(rs.next()){
        Evenement e;
            e = new Evenement(rs.getInt("idevent"),rs.getString("nom"),rs.getString("local"),rs.getString("date"));
        list.add(e) ;
        }
        return list ;
    }
    
public boolean delete1(int i) throws SQLException {
          PreparedStatement pre=con.prepareStatement("delete from event WHERE idevent='"+i+"';");
            pre.executeUpdate();
            return true;
    }
    
}
