/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.larvard.Service;

import com.larvard.Entity.Activite;
import com.larvard.Entity.AfterSchool;
import com.larvard.IService.IServiceAfterSchool;
import com.larvard.utils.DataBase;
import java.sql.Connection ;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List; 
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ivaxs
 */
public class ServiceAfterSchool implements IServiceAfterSchool<AfterSchool> {
     private final Connection con;
    private Statement ste;

    public ServiceAfterSchool() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void ajouter(AfterSchool as) throws SQLException , ParseException {
        //SimpleDateFormat format = new SimpleDateFormat( "MM/dd/yyyy" );  // United States style of format.
//java.util.Date myDate = format.parse( as.getDateSchool());  // Notice the ".util." of package name.
        PreparedStatement pre=con.prepareStatement("INSERT INTO `pidev`.`afterschool` ( `IDafterschool`, `IDactivite`, `dateafterschool`) VALUES ( NULL,?,?);");
    pre.setInt(1,as.getActivite());

    ///java.sql.Date sqlDate = new java.sql.Date( myDate.getTime() ); // Notice the ".sql." (not "util") in package name.
    pre.setDate(2,(Date) as.getDateSchool() );
    pre.executeUpdate();
    }

    @Override
    public void delete(int t) throws SQLException {
     
            String requete = "DELETE from afterschool where IDafterschool=?";
            try {
            PreparedStatement st =con.prepareStatement(requete);
            st.setInt(1,t);
            st.executeUpdate();
            System.out.println("Afterschool Supprimee.");

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    @Override
    public List<AfterSchool> readAll() throws SQLException {
    List<AfterSchool> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from afterschool");
     while (rs.next()) { 
         int id=rs.getInt("IDafterschool");
        int act =rs.getInt("IDactivite") ;
      
               Date dateafschool=rs.getDate("dateafterschool");
               
              AfterSchool as=new AfterSchool( id,act,dateafschool);
               
     arr.add(as);
     }
    return arr;
    }
        @Override 
    public List<Integer> readid() {
            List<Integer> arr=new ArrayList<>();
            try{
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from afterschool");
     while (rs.next()) {                
              // int IDActivite=rs.getInt(1);
                Integer titre=rs.getInt("IDafterschool");
              // String description=rs.getString("descriptionAct");
               //int duree = rs.getInt("duree") ; 
              // String type=rs.getString("type");
              //Activite act=new Activite(titre);
               
     arr.add(titre);
     }
            }catch(SQLException ex){
                
            }
    return arr;
    }
    
    
    
    
   
}
